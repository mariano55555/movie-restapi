# Endpoints


## docs.json




> Example request:

```bash
curl -X GET \
    -G "http://localhost/docs.json" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/docs.json"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "variables": [],
    "info": {
        "name": "API RESTFUL TEST API",
        "_postman_id": "e20a13d8-cd84-4d6a-a771-d62db3e02283",
        "description": "",
        "schema": "https:\/\/schema.getpostman.com\/json\/collection\/v2.0.0\/collection.json"
    },
    "item": [
        {
            "name": "Auth endpoints",
            "description": "\nAuthentication methods: Register, Login, Logout, Refresh Token and Me",
            "item": [
                {
                    "name": "Registration",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/register",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"name\": \"et\",\n    \"email\": \"sequi\",\n    \"password\": \"sint\",\n    \"password_confirmation\": \"aliquid\"\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Creates a user with role \"User\" (It doesnt allow to chose a role)",
                        "response": []
                    }
                },
                {
                    "name": "Login",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/login",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"email\": \"mariano.paz.flores@gmail.com\",\n    \"password\": \"pariatur\"\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Login Method to allow users to use all available endpoints through an access token",
                        "response": []
                    }
                },
                {
                    "name": "Refresh",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/refresh",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Refresh token",
                        "response": []
                    }
                },
                {
                    "name": "Logout",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/logout",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Logout for aunthenticated users",
                        "response": []
                    }
                },
                {
                    "name": "Me",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/me",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Get the authenticated User.",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Endpoints",
            "description": "",
            "item": [
                {
                    "name": "docs.json",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "docs.json",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Send a reset link to the given user.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/password\/email",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Reset the given user's password.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/password\/reset",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Resend the email verification notification.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/email\/resend",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Mark the authenticated user's email address as verified.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/email\/verify\/:id\/:hash",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Display a listing of the resource.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Store a newly created resource in storage.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Display the specified resource.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Update the specified resource in storage.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user",
                            "query": []
                        },
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Remove the specified resource from storage.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user",
                            "query": []
                        },
                        "method": "DELETE",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Likes endpoints",
            "description": "\nMovie methods available:\n\nFor Regular Users and Admins: Store",
            "item": [
                {
                    "name": "Likes",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user\/movies\/:movie\/likes",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "User likes\/Unlike a movie",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Movie endpoints",
            "description": "\nMovie methods available:\n\nFor Regular Users, Admin and Guests: Index, Show\nFor Admin Users only: Store, Update and Destroy",
            "item": [
                {
                    "name": "Listing Movies",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies",
                            "query": [
                                {
                                    "key": "sortBy",
                                    "value": "dolorum",
                                    "description": "to sort by. Defaults to 'name'.",
                                    "disabled": false
                                },
                                {
                                    "key": "sortByDesc",
                                    "value": "ad",
                                    "description": "to sort by desc",
                                    "disabled": false
                                },
                                {
                                    "key": "per_page",
                                    "value": "mollitia",
                                    "description": "to paginate the listing. Default to 15",
                                    "disabled": false
                                },
                                {
                                    "key": "filters:",
                                    "value": "name%3D%22Autem%22",
                                    "description": "add the name of the field follow by colon and the value.",
                                    "disabled": false
                                }
                            ]
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Display a listing of movies with pagination, filters, searching, etc.",
                        "response": []
                    }
                },
                {
                    "name": "Create Movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "multipart\/form-data"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "formdata",
                            "formdata": [
                                {
                                    "key": "title",
                                    "value": "vel",
                                    "type": "text"
                                },
                                {
                                    "key": "description",
                                    "value": "hic",
                                    "type": "text"
                                },
                                {
                                    "key": "available",
                                    "value": "unavailable",
                                    "type": "text"
                                },
                                {
                                    "key": "rental_price",
                                    "value": 1726994.38,
                                    "type": "text"
                                },
                                {
                                    "key": "sale_price",
                                    "value": 46.01566,
                                    "type": "text"
                                },
                                {
                                    "key": "daily_penalty",
                                    "value": 710977346.34,
                                    "type": "text"
                                },
                                {
                                    "key": "poster",
                                    "src": [],
                                    "type": "file"
                                }
                            ]
                        },
                        "description": "Store a newly created movie in storage. (Only admin users)",
                        "response": []
                    }
                },
                {
                    "name": "Show Movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies\/:movie",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Display the specified movie.",
                        "response": []
                    }
                },
                {
                    "name": "Update Movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies\/:movie",
                            "query": []
                        },
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "multipart\/form-data"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "formdata",
                            "formdata": [
                                {
                                    "key": "name",
                                    "value": "Titanic",
                                    "type": "text"
                                },
                                {
                                    "key": "synopsis",
                                    "value": "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
                                    "type": "text"
                                },
                                {
                                    "key": "status",
                                    "value": "Available\nThe value must be one of available, unavailable, Available, or Unavailable.",
                                    "type": "text"
                                },
                                {
                                    "key": "rent_daily",
                                    "value": "1.00",
                                    "type": "text"
                                },
                                {
                                    "key": "price_sale",
                                    "value": "2.5",
                                    "type": "text"
                                },
                                {
                                    "key": "penalty_per_day",
                                    "value": "1.25",
                                    "type": "text"
                                },
                                {
                                    "key": "poster",
                                    "src": [],
                                    "type": "file"
                                }
                            ]
                        },
                        "description": "Update the specified movie in storage. You need to at least update 1 field. (Only admin users)",
                        "response": []
                    }
                },
                {
                    "name": "Delete a movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies\/:movie",
                            "query": []
                        },
                        "method": "DELETE",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Remove the specified movie from storage (Only admin users)",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Rentals(Transaction) endpoints",
            "description": "\nMovie methods available:\n\nFor Regular Users, Admin and Guests: Show, Update\nFor Admin Users only: Index,",
            "item": [
                {
                    "name": "Listing rentals",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/rentals",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "List of all transactions available (Only admin users)",
                        "response": []
                    }
                },
                {
                    "name": "Show Rental",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/rentals\/:rental",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Display the specified transaction.",
                        "response": []
                    }
                },
                {
                    "name": "Update Rental",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/rentals\/:rental",
                            "query": []
                        },
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"qty\": 4286941.09093,\n    \"transaction_type\": \"purchase\",\n    \"return_date\": \"2020-06-13 16:21:18\",\n    \"transaction\": \"Rent\"\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Update specified transaction.",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Transaction endpoint",
            "description": "",
            "item": [
                {
                    "name": "Create a Rental\/Transaction",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user\/movies\/:movie\/rentals",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"transaction\": \"Rent\",\n    \"qty\": 283212.41706421\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "It generates a rental\/purchase for an especific user",
                        "response": []
                    }
                }
            ]
        }
    ]
}
```

### Request
<small class="badge badge-green">GET</small>
 **`docs.json`**



## Send a reset link to the given user.




> Example request:

```bash
curl -X POST \
    "http://localhost/api/password/email" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/password/email"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### Request
<small class="badge badge-black">POST</small>
 **`api/password/email`**



## Reset the given user&#039;s password.




> Example request:

```bash
curl -X POST \
    "http://localhost/api/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### Request
<small class="badge badge-black">POST</small>
 **`api/password/reset`**



## Resend the email verification notification.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/email/resend" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email/resend"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated.",
    "code": 401
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/email/resend`**



## Mark the authenticated user&#039;s email address as verified.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/email/verify/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/email/verify/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (403):

```json
{
    "error": "Forbidden",
    "code": 403
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/email/verify/{id}/{hash}`**



## Display a listing of the resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated.",
    "code": 401
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/users`**



## Store a newly created resource in storage.




> Example request:

```bash
curl -X POST \
    "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### Request
<small class="badge badge-black">POST</small>
 **`api/users`**



## Display the specified resource.




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated.",
    "code": 401
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/users/{user}`**



## Update the specified resource in storage.




> Example request:

```bash
curl -X PUT \
    "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### Request
<small class="badge badge-darkblue">PUT</small>
 **`api/users/{user}`**

<small class="badge badge-purple">PATCH</small>
 **`api/users/{user}`**



## Remove the specified resource from storage.




> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### Request
<small class="badge badge-red">DELETE</small>
 **`api/users/{user}`**




