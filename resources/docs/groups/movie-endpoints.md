# Movie endpoints

Movie methods available:

For Regular Users, Admin and Guests: Index, Show
For Admin Users only: Store, Update and Destroy

## Listing Movies


Display a listing of movies with pagination, filters, searching, etc.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/movies?sortBy=porro&sortByDesc=nisi&per_page=nihil&filters%3A=name%3D%22Autem%22" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/movies"
);

let params = {
    "sortBy": "porro",
    "sortByDesc": "nisi",
    "per_page": "nihil",
    "filters:": "name="Autem"",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
     "data": {
          {
              "resource": 33,
              "name": "Autem placeat voluptatibus fugit.",
              "synopsis": "Ipsum ullam dignissimos incidunt. Eligendi a consequuntur ipsa illum et quam vero. Voluptatibus animi sed veritatis repellat at repellendus dignissimos voluptas. Error corrupti sed est corporis esse.",
              "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
              "likes": 2,
              "rent_daily": 1.8,
              "price_sale": 7.6,
              "penalty_per_day": 1.5,
              "created_at": "2020-06-12 21:44:12",
              "updated_at": "2020-06-12 21:44:12",
              "links": [
                  {
                      "rel": "self",
                      "href": "http:\/\/localhost\/api\/movies\/33"
                  }
              ]
          },
          {
              "resource": 22,
              "name": "Debitis dolore totam labore omnis asperiores dolore necessitatibus.",
              "synopsis": "Nulla eos doloremque rerum maiores. Veniam qui repellendus deleniti nemo provident. Quia quod soluta blanditiis nihil quos et. Vero voluptas officia voluptatum consequatur. Et veniam doloribus quia aut. Et quis explicabo voluptatibus accusamus voluptatem est.",
              "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
              "likes": 0,
              "rent_daily": 1.8,
              "price_sale": 7.1,
              "penalty_per_day": 1.4,
              "created_at": "2020-06-12 21:44:12",
              "updated_at": "2020-06-12 21:44:12",
              "links": [
                  {
                      "rel": "self",
                      "href": "http:\/\/localhost\/api\/movies\/22"
                  }
              ]
          },
          {
              "resource": 10,
              "name": "Distinctio eaque fugit assumenda illum.",
              "synopsis": "Doloribus et assumenda excepturi minima totam odio omnis. Ea numquam tempore dicta facere. Quos modi distinctio fugit quasi et consequatur. Qui sunt exercitationem quisquam eum est. Ut suscipit eum dolor in rerum laborum est. Doloremque sed atque harum iste at. Ea eum occaecati at.",
              "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
              "likes": 0,
              "rent_daily": 1.3,
              "price_sale": 5.5,
              "penalty_per_day": 1,
              "created_at": "2020-06-12 21:44:12",
              "updated_at": "2020-06-12 21:44:12",
              "links": [
                  {
                      "rel": "self",
                      "href": "http:\/\/localhost\/api\/movies\/10"
                  }
              ]
          },
     },
     "pagination": {
         "current_page": 1,
         "from": 1,
         "last_page": 2,
         "per_page": 15,
         "to": 15,
         "total": 21,
         "count": 15,
         "prev": null,
         "next": "http:\/\/localhost\/api\/movies?page=2",
         "first": "http:\/\/localhost\/api\/movies?page=1",
         "last": "http:\/\/localhost\/api\/movies?page=2"
     }
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/movies`**

<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<code><b>sortBy</b></code>&nbsp;          <i>optional</i>    <br>
    to sort by. Defaults to 'name'.

<code><b>sortByDesc</b></code>&nbsp;          <i>optional</i>    <br>
    to sort by desc

<code><b>per_page</b></code>&nbsp;          <i>optional</i>    <br>
    to paginate the listing. Default to 15

<code><b>filters:</b></code>&nbsp;          <i>optional</i>    <br>
    add the name of the field follow by colon and the value.



## Create Movie

<small class="badge badge-darkred">requires authentication</small>

Store a newly created movie in storage. (Only admin users)

> Example request:

```bash
curl -X POST \
    "http://localhost/api/movies" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -F "title=accusamus" \
    -F "description=quis" \
    -F "available=Unavailable" \
    -F "rental_price=71965774.142753" \
    -F "sale_price=2811784.549586" \
    -F "daily_penalty=1.21608322" \
    -F "poster=@C:\Users\squeaky\AppData\Local\Temp\phpC75D.tmp" 
```

```javascript
const url = new URL(
    "http://localhost/api/movies"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

const body = new FormData();
body.append('title', 'accusamus');
body.append('description', 'quis');
body.append('available', 'Unavailable');
body.append('rental_price', '71965774.142753');
body.append('sale_price', '2811784.549586');
body.append('daily_penalty', '1.21608322');
body.append('poster', document.querySelector('input[name="poster"]').files[0]);

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
"data": {
  "resource": 82,
  "name": "New Movie 2020",
  "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
  "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
  "likes": 0,
  "status": "Unavailable",
  "creator": "Bernard Abshire",
  "editor": "Bernard Abshire",
  "rent_daily": 1.25,
  "price_sale": 15.45,
  "penalty_per_day": 1,
  "created_at": "2020-06-13 15:14:34",
  "updated_at": "2020-06-13 15:14:34",
  "links": [
      {
          "rel": "self",
          "href": "http://localhost:8000/api/movies/82"
      }
  ]
}
```
> Example response (422, Validation Failed):

```json
{
    "error": {
        "name": [
            "The name has already been taken."
        ]
    },
    "code": 422
}
```

### Request
<small class="badge badge-black">POST</small>
 **`api/movies`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>title</b></code>&nbsp; <small>string</small>     <br>
    

<code><b>poster</b></code>&nbsp; <small>file</small>         <i>optional</i>    <br>
    The value must be an image.

<code><b>description</b></code>&nbsp; <small>string</small>     <br>
    

<code><b>available</b></code>&nbsp; <small>string</small>     <br>
    The value must be one of <code>available</code>, <code>unavailable</code>, <code>Available</code>, or <code>Unavailable</code>.

<code><b>rental_price</b></code>&nbsp; <small>number</small>     <br>
    

<code><b>sale_price</b></code>&nbsp; <small>number</small>     <br>
    

<code><b>daily_penalty</b></code>&nbsp; <small>number</small>     <br>
    



## Show Movie


Display the specified movie.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/movies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/movies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404, Not Found):

```json
{
    "error": "Does not exist any instance of movie with the given id",
    "code": 404
}
```
> Example response (200):

```json
{
    "data": {
        "resource": 1,
        "name": "Excepturi ut exercitationem eos deserunt quisquam.",
        "synopsis": "Aut asperiores omnis possimus velit quisquam error. Deleniti debitis asperiores quia possimus aut. Exercitationem inventore eligendi quia repudiandae. Omnis sit qui quas voluptatum. Consequatur molestiae reprehenderit non aliquam. Debitis ratione assumenda id nulla possimus. Minus voluptas enim odio temporibus quod illo laudantium.",
        "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
        "likes": 1,
        "rent_daily": 1.7,
        "price_sale": 9.3,
        "penalty_per_day": 1.1,
        "created_at": "2020-06-12 21:44:12",
        "updated_at": "2020-06-12 21:44:12",
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost\/api\/movies\/1"
            }
        ]
    }
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/movies/{movie}`**



## Update Movie

<small class="badge badge-darkred">requires authentication</small>

Update the specified movie in storage. You need to at least update 1 field. (Only admin users)

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/movies/1" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -F "name=Titanic" \
    -F "synopsis=Lorem ipsum dolor sit, amet consectetur adipisicing elit." \
    -F "status=Available
The value must be one of available, unavailable, Available, or Unavailable." \
    -F "rent_daily=1.00" \
    -F "price_sale=2.5" \
    -F "penalty_per_day=1.25" \
    -F "poster=@C:\Users\squeaky\AppData\Local\Temp\phpC79C.tmp" 
```

```javascript
const url = new URL(
    "http://localhost/api/movies/1"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

const body = new FormData();
body.append('name', 'Titanic');
body.append('synopsis', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.');
body.append('status', 'Available
The value must be one of available, unavailable, Available, or Unavailable.');
body.append('rent_daily', '1.00');
body.append('price_sale', '2.5');
body.append('penalty_per_day', '1.25');
body.append('poster', document.querySelector('input[name="poster"]').files[0]);

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
"data": {
  "resource": 82,
  "name": "New Movie 2020",
  "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
  "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
  "likes": 0,
  "status": "Unavailable",
  "creator": "Bernard Abshire",
  "editor": "Bernard Abshire",
  "rent_daily": 1.25,
  "price_sale": 15.45,
  "penalty_per_day": 1,
  "created_at": "2020-06-13 15:14:34",
  "updated_at": "2020-06-13 15:14:34",
  "links": [
      {
          "rel": "self",
          "href": "http://localhost:8000/api/movies/82"
      }
  ]
}
```
> Example response (422, Validation Failed):

```json
{
    "error": {
        "name": [
            "The name has already been taken."
        ]
    },
    "code": 422
}
```
> Example response (404, Not Found):

```json
{
    "error": "Does not exist any instance of movie with the given id",
    "code": 404
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`api/movies/{movie}`**

<small class="badge badge-purple">PATCH</small>
 **`api/movies/{movie}`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>name</b></code>&nbsp; <small>string.</small>         <i>optional</i>    <br>
    

<code><b>poster</b></code>&nbsp; <small>file</small>         <i>optional</i>    <br>
    The value must be an image.

<code><b>synopsis</b></code>&nbsp; <small>string.</small>         <i>optional</i>    <br>
    

<code><b>status</b></code>&nbsp; <small>string.</small>         <i>optional</i>    <br>
    

<code><b>rent_daily</b></code>&nbsp; <small>number.</small>         <i>optional</i>    <br>
    

<code><b>price_sale</b></code>&nbsp; <small>number.</small>         <i>optional</i>    <br>
    

<code><b>penalty_per_day</b></code>&nbsp; <small>number.</small>         <i>optional</i>    <br>
    



## Delete a movie

<small class="badge badge-darkred">requires authentication</small>

Remove the specified movie from storage (Only admin users)

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/movies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/movies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
"data": {
  "resource": 82,
  "name": "New Movie 2020",
  "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
  "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
  "likes": 0,
  "status": "Unavailable",
  "creator": "Bernard Abshire",
  "editor": "Bernard Abshire",
  "rent_daily": 1.25,
  "price_sale": 15.45,
  "penalty_per_day": 1,
  "created_at": "2020-06-13 15:14:34",
  "updated_at": "2020-06-13 15:14:34",
  "links": [
      {
          "rel": "self",
          "href": "http://localhost:8000/api/movies/82"
      }
  ]
}
```
> Example response (404, Not Found):

```json
{
    "error": "Does not exist any instance of movie with the given id",
    "code": 404
}
```

### Request
<small class="badge badge-red">DELETE</small>
 **`api/movies/{movie}`**




