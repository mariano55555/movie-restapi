# Rentals(Transaction) endpoints

Movie methods available:

For Regular Users, Admin and Guests: Show, Update
For Admin Users only: Index,

## Listing rentals

<small class="badge badge-darkred">requires authentication</small>

List of all transactions available (Only admin users)

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/rentals" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/rentals"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "resource": 1,
            "transaction": "RENT",
            "qty": 1,
            "creator": "Ramona Barrows",
            "editor": "Ramona Barrows",
            "rent_date": "2020-06-12 21:44:17",
            "due_date": "2020-06-15 21:44:17",
            "return_date": "2020-06-16 04:00:15",
            "daily_penalty": 1,
            "rental_price": 1.2,
            "movie": {
                "resource": 50,
                "name": "Mariano Paz",
                "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 0,
                "status": "Unavailable",
                "creator": "System",
                "editor": "Ramona Barrows",
                "rent_daily": 4.8,
                "price_sale": 2.2,
                "penalty_per_day": 1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:57:35",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/50"
                    }
                ]
            },
            "customer": {
                "resource": 5,
                "full_name": "Mr. Sylvester Murphy V",
                "email": "mcclure.ross@example.org",
                "role": "User",
                "creator": "System",
                "editor": "System",
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/users\/5"
                    }
                ]
            },
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/rentals\/1"
                }
            ]
        },
        {
            "resource": 2,
            "transaction": "PURCHASE",
            "qty": 2,
            "creator": "Bernard Abshire",
            "editor": "Bernard Abshire",
            "sale_price": 2.2,
            "movie": {
                "resource": 50,
                "name": "Mariano Paz",
                "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 0,
                "status": "Unavailable",
                "creator": "System",
                "editor": "Ramona Barrows",
                "rent_daily": 4.8,
                "price_sale": 2.2,
                "penalty_per_day": 1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:57:35",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/50"
                    }
                ]
            },
            "customer": {
                "resource": 3,
                "full_name": "Bernard Abshire",
                "email": "lavon.stiedemann@example.org",
                "role": "Admin",
                "creator": "System",
                "editor": "System",
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/users\/3"
                    }
                ]
            },
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/rentals\/2"
                }
            ]
        },
        {
            "resource": 3,
            "transaction": "RENT",
            "qty": 1,
            "creator": "Bernard Abshire",
            "editor": "Bernard Abshire",
            "rent_date": "2020-06-13 02:20:15",
            "due_date": "2020-06-16 02:20:15",
            "return_date": "2020-06-18 04:00:12",
            "daily_penalty": 1.5,
            "rental_price": 1.6,
            "movie": {
                "resource": 12,
                "name": "Et rerum quaerat omnis ab est vero.",
                "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 0,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.6,
                "price_sale": 6.1,
                "penalty_per_day": 1.5,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/12"
                    }
                ]
            },
            "customer": {
                "resource": 3,
                "full_name": "Bernard Abshire",
                "email": "lavon.stiedemann@example.org",
                "role": "Admin",
                "creator": "System",
                "editor": "System",
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/users\/3"
                    }
                ]
            },
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/rentals\/3"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/localhost:8000\/api\/rentals?page=1",
        "last": "http:\/\/localhost:8000\/api\/rentals?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost:8000\/api\/rentals",
        "per_page": 15,
        "to": 3,
        "total": 3
    }
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/rentals`**



## Show Rental

<small class="badge badge-darkred">requires authentication</small>

Display the specified transaction.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/rentals/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/rentals/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "resource": 3,
        "transaction": "RENT",
        "qty": 1,
        "creator": "Bernard Abshire",
        "editor": "Bernard Abshire",
        "rent_date": "2020-06-13 02:20:15",
        "due_date": "2020-06-16 02:20:15",
        "return_date": "2020-06-18 04:00:12",
        "daily_penalty": 1.5,
        "rental_price": 1.6,
        "movie": {
            "resource": 12,
            "name": "Et rerum quaerat omnis ab est vero.",
            "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
            "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
            "likes": 0,
            "status": "Unavailable",
            "creator": "System",
            "editor": "System",
            "rent_daily": 1.6,
            "price_sale": 6.1,
            "penalty_per_day": 1.5,
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/movies\/12"
                }
            ]
        },
        "customer": {
            "resource": 3,
            "full_name": "Bernard Abshire",
            "email": "lavon.stiedemann@example.org",
            "role": "Admin",
            "creator": "System",
            "editor": "System",
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/3"
                }
            ]
        },
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/rentals\/3"
            }
        ]
    }
}
```
> Example response (404, Not Found):

```json
{
    "error": "Does not exist any instance of rentals with the given id",
    "code": 404
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/rentals/{rental}`**



## Update Rental

<small class="badge badge-darkred">requires authentication</small>

Update specified transaction.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/rentals/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -d '{"transaction":"Rent","qty":351456061.82198256}'

```

```javascript
const url = new URL(
    "http://localhost/api/rentals/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

let body = {
    "transaction": "Rent",
    "qty": 351456061.82198256
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "resource": 2,
        "transaction": "PURCHASE",
        "qty": 2,
        "creator": "Bernard Abshire",
        "editor": "Bernard Abshire",
        "sale_price": 2.2,
        "movie": {
            "resource": 50,
            "name": "Mariano Paz",
            "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
            "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
            "likes": 0,
            "status": "Unavailable",
            "creator": "System",
            "editor": "Ramona Barrows",
            "rent_daily": 4.8,
            "price_sale": 2.2,
            "penalty_per_day": 1,
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:57:35",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/movies\/50"
                }
            ]
        },
        "customer": {
            "resource": 3,
            "full_name": "Bernard Abshire",
            "email": "lavon.stiedemann@example.org",
            "role": "Admin",
            "creator": "System",
            "editor": "System",
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/3"
                }
            ]
        },
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/rentals\/2"
            }
        ]
    }
}
```
> Example response (404, Not Found):

```json
{
    "error": "Does not exist any instance of rentals with the given id",
    "code": 404
}
```

### Request
<small class="badge badge-darkblue">PUT</small>
 **`api/rentals/{rental}`**

<small class="badge badge-purple">PATCH</small>
 **`api/rentals/{rental}`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>transaction</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
    optional Type of transaction value in <code>rent</code>, <code>RENT</code>, <code>Rent</code>,<code>Purchase</code>, <code>PURCHASE</code>, <code>purchase</code>.

<code><b>qty</b></code>&nbsp; <small>number</small>         <i>optional</i>    <br>
    optional quantity of movies for purchasing or renting.




