# Transaction endpoint


## Create a Rental/Transaction

<small class="badge badge-darkred">requires authentication</small>

It generates a rental/purchase for an especific user

> Example request:

```bash
curl -X POST \
    "http://localhost/api/users/1/movies/1/rentals" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -d '{"transaction":"Rent","qty":262.68}'

```

```javascript
const url = new URL(
    "http://localhost/api/users/1/movies/1/rentals"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

let body = {
    "transaction": "Rent",
    "qty": 262.68
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "resource": 4,
        "transaction": "RENT",
        "qty": 1,
        "creator": "Bernard Abshire",
        "editor": "Bernard Abshire",
        "rent_date": "2020-06-13T16:05:20.823672Z",
        "due_date": "2020-06-16T16:05:20.823672Z",
        "return_date": "",
        "daily_penalty": 1,
        "rental_price": 1.25,
        "movie": {
            "resource": 82,
            "name": "New Movie 2020",
            "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
            "image": "http:\/\/localhost:8000\/img\/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
            "likes": 0,
            "status": "Unavailable",
            "creator": "Bernard Abshire",
            "editor": "Bernard Abshire",
            "rent_daily": 1.25,
            "price_sale": 15.45,
            "penalty_per_day": 1,
            "created_at": "2020-06-13 15:14:34",
            "updated_at": "2020-06-13 15:14:34",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/movies\/82"
                }
            ]
        },
        "customer": {
            "resource": 3,
            "full_name": "Bernard Abshire",
            "email": "lavon.stiedemann@example.org",
            "role": "Admin",
            "creator": "System",
            "editor": "System",
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/3"
                }
            ]
        },
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/rentals\/4"
            }
        ]
    }
}
```

### Request
<small class="badge badge-black">POST</small>
 **`api/users/{user}/movies/{movie}/rentals`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>transaction</b></code>&nbsp; <small>string</small>     <br>
    Type of transaction value in <code>rent</code>, <code>RENT</code>, <code>Rent</code>,<code>Purchase</code>, <code>PURCHASE</code>, <code>purchase</code>.

<code><b>qty</b></code>&nbsp; <small>number</small>     <br>
    quantity of movies for purchasing or renting.




