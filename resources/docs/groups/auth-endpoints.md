# Auth endpoints

Authentication methods: Register, Login, Logout, Refresh Token and Me

## Registration


Creates a user with role "User" (It doesnt allow to chose a role)

> Example request:

```bash
curl -X POST \
    "http://localhost/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"aut","email":"debitis","password":"saepe","password_confirmation":"inventore"}'

```

```javascript
const url = new URL(
    "http://localhost/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "aut",
    "email": "debitis",
    "password": "saepe",
    "password_confirmation": "inventore"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (201):

```json
{
    "data": {
        "user": {
            "resource": 12,
            "full_name": "mariansopfaasdfa",
            "email": "mariaasdnoasdfa1asdfa2@marianosa.com",
            "created_at": "2020-06-13 14:44:30",
            "updated_at": "2020-06-13 14:44:30",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/12"
                }
            ]
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJA..."
    }
}
```
> Example response (422, Validation Failed):

```json
{
    "error": {
        "password": [
            "The password field is required."
        ]
    },
    "code": 422
}
```

### Request
<small class="badge badge-black">POST</small>
 **`api/register`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>name</b></code>&nbsp; <small>string</small>     <br>
    The name of the user.

<code><b>email</b></code>&nbsp; <small>string</small>     <br>
    The email of the user.

<code><b>password</b></code>&nbsp; <small>string</small>     <br>
    The password.

<code><b>password_confirmation</b></code>&nbsp; <small>string</small>     <br>
    the confirmation of the password



## Login


Login Method to allow users to use all available endpoints through an access token

> Example request:

```bash
curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -d '{"email":"mariano.paz.flores@gmail.com","password":"ducimus"}'

```

```javascript
const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

let body = {
    "email": "mariano.paz.flores@gmail.com",
    "password": "ducimus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "user": {
            "resource": 12,
            "full_name": "mariansopfaasdfa",
            "email": "mariaasdnoasdfa1asdfa2@marianosa.com",
            "created_at": "2020-06-13 14:44:30",
            "updated_at": "2020-06-13 14:44:30",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/12"
                }
            ]
        },
        "access_token": "eyJ0eXAiOiJKV1QiLC..."
    }
}
```
> Example response (422, Validation Failed):

```json
{
    "error": {
        "name": [
            "The name field is required."
        ],
        "email": [
            "The email field is required."
        ],
        "password": [
            "The password field is required."
        ]
    },
    "code": 422
}
```

### Request
<small class="badge badge-black">POST</small>
 **`api/login`**

<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<code><b>email</b></code>&nbsp; <small>string</small>     <br>
    The email of the user.

<code><b>password</b></code>&nbsp; <small>string</small>     <br>
    The password of the user.



## Refresh

<small class="badge badge-darkred">requires authentication</small>

Refresh token

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/refresh" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/refresh"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
     data {
         'token':'eyJ0eXAiOiJKV1QiLC...'
     }
}
```
> Example response (401, Unauthenticated):

```json

{
 "error": {
    "error": "Unauthenticated.",
    "code": 401
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/refresh`**



## Logout

<small class="badge badge-darkred">requires authentication</small>

Logout for aunthenticated users

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
     data {
         'message':'Successfully logged out'
     }
}
```

### Request
<small class="badge badge-green">GET</small>
 **`api/logout`**



## Me

<small class="badge badge-darkred">requires authentication</small>

Get the authenticated User.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json

{
     data: {
          "resource": 3,
          "full_name": "Bernard Abshire",
          "email": "lavon.stiedemann@example.org",
          "role": "Admin",
          "creator": "System",
          "editor": "System",
          "created_at": "2020-06-12 21:44:12",
          "updated_at": "2020-06-12 21:44:12",
          "links": [
              {
                  "rel": "self",
                  "href": "http://localhost:8000/api/users/3"
              }
          ]
      }
}
```
> Example response (401, Unauthenticated):

```json

{
 "error": {
    "error": "Unauthenticated.",
    "code": 401
}
```

### Request
<small class="badge badge-black">POST</small>
 **`api/me`**




