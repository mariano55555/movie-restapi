# Likes endpoints

Movie methods available:

For Regular Users and Admins: Store

## Likes

<small class="badge badge-darkred">requires authentication</small>

User likes/Unlike a movie

> Example request:

```bash
curl -X POST \
    "http://localhost/api/users/1/movies/1/likes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
```

```javascript
const url = new URL(
    "http://localhost/api/users/1/movies/1/likes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};


fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "resource": 3,
        "user": "Bernard Abshire",
        "role": "Admin",
        "creator": "System",
        "editor": "System",
        "created_at": "2020-06-12 21:44:12",
        "updated_at": "2020-06-12 21:44:12",
        "likes": [
            {
                "resource": 1,
                "name": "Excepturi ut exercitationem eos deserunt quisquam.",
                "synopsis": "Aut asperiores omnis possimus velit quisquam error. Deleniti debitis asperiores quia possimus aut. Exercitationem inventore eligendi quia repudiandae. Omnis sit qui quas voluptatum. Consequatur molestiae reprehenderit non aliquam. Debitis ratione assumenda id nulla possimus. Minus voluptas enim odio temporibus quod illo laudantium.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.7,
                "price_sale": 9.3,
                "penalty_per_day": 1.1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/1"
                    }
                ]
            },
            {
                "resource": 12,
                "name": "Et rerum quaerat omnis ab est vero.",
                "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.6,
                "price_sale": 6.1,
                "penalty_per_day": 1.5,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/12"
                    }
                ]
            },
            {
                "resource": 26,
                "name": "Sit totam officia debitis.",
                "synopsis": "Quidem itaque quae qui nemo quis voluptas minus accusamus. Ad est et similique rerum consectetur. Qui ducimus quisquam inventore blanditiis odit laborum ipsa. At aliquam numquam rerum enim voluptas.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Available",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.5,
                "price_sale": 8.6,
                "penalty_per_day": 1.2,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/26"
                    }
                ]
            },
            {
                "resource": 33,
                "name": "Autem placeat voluptatibus fugit.",
                "synopsis": "Ipsum ullam dignissimos incidunt. Eligendi a consequuntur ipsa illum et quam vero. Voluptatibus animi sed veritatis repellat at repellendus dignissimos voluptas. Error corrupti sed est corporis esse.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 2,
                "status": "Available",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.8,
                "price_sale": 7.6,
                "penalty_per_day": 1.5,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/33"
                    }
                ]
            },
            {
                "resource": 39,
                "name": "Possimus unde cumque esse saepe nulla vel.",
                "synopsis": "Alias reprehenderit iste accusantium qui molestiae dolorem reprehenderit. Et ea et voluptas fugit molestiae molestiae numquam. Omnis neque est iusto labore. Nemo ratione aliquam sunt. Soluta enim ea temporibus et labore numquam saepe necessitatibus. Magnam molestiae aperiam ut aut. Itaque quis non molestiae maiores earum in.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.6,
                "price_sale": 8.5,
                "penalty_per_day": 1.4,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/39"
                    }
                ]
            },
            {
                "resource": 50,
                "name": "Mariano Paz",
                "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 2,
                "status": "Unavailable",
                "creator": "System",
                "editor": "Ramona Barrows",
                "rent_daily": 4.8,
                "price_sale": 2.2,
                "penalty_per_day": 1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:57:35",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/50"
                    }
                ]
            }
        ],
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/users\/3"
            }
        ]
    }
}
```

### Request
<small class="badge badge-black">POST</small>
 **`api/users/{user}/movies/{movie}/likes`**




