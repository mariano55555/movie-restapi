<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>API Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/style.css") }}" media="screen" />
        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/print.css") }}" media="print" />
        <script src="{{ asset("vendor/scribe/js/all.js") }}"></script>

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/highlight-darcula.css") }}" media="" />
        <script src="{{ asset("vendor/scribe/js/highlight.pack.js") }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body class="" data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">
<a href="#" id="nav-button">
      <span>
        NAV
            <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="-image" class=""/>
      </span>
</a>
<div class="tocify-wrapper">
                <div class="lang-selector">
                            <a href="#" data-language-name="bash">bash</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.json") }}">View Postman Collection</a></li>
                            <li><a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: June 13 2020</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>Welcome to our API documentation!</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile), and you can switch the programming language of the examples with the tabs in the top right (or from the nav menu at the top left on mobile).</aside><h1>Authenticating requests</h1>
<p>This API is not authenticated.</p><h1>Auth endpoints</h1>
<p>Authentication methods: Register, Login, Logout, Refresh Token and Me</p>
<h2>Registration</h2>
<p>Creates a user with role &quot;User&quot; (It doesnt allow to chose a role)</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"aut","email":"debitis","password":"saepe","password_confirmation":"inventore"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "aut",
    "email": "debitis",
    "password": "saepe",
    "password_confirmation": "inventore"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "user": {
            "resource": 12,
            "full_name": "mariansopfaasdfa",
            "email": "mariaasdnoasdfa1asdfa2@marianosa.com",
            "created_at": "2020-06-13 14:44:30",
            "updated_at": "2020-06-13 14:44:30",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/12"
                }
            ]
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJA..."
    }
}</code></pre>
<blockquote>
<p>Example response (422, Validation Failed):</p>
</blockquote>
<pre><code class="language-json">{
    "error": {
        "password": [
            "The password field is required."
        ]
    },
    "code": 422
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/register</code></strong></p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p><code><b>name</b></code>&nbsp; <small>string</small>     <br>
The name of the user.</p>
<p><code><b>email</b></code>&nbsp; <small>string</small>     <br>
The email of the user.</p>
<p><code><b>password</b></code>&nbsp; <small>string</small>     <br>
The password.</p>
<p><code><b>password_confirmation</b></code>&nbsp; <small>string</small>     <br>
the confirmation of the password</p>
<h2>Login</h2>
<p>Login Method to allow users to use all available endpoints through an access token</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -d '{"email":"mariano.paz.flores@gmail.com","password":"ducimus"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

let body = {
    "email": "mariano.paz.flores@gmail.com",
    "password": "ducimus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "user": {
            "resource": 12,
            "full_name": "mariansopfaasdfa",
            "email": "mariaasdnoasdfa1asdfa2@marianosa.com",
            "created_at": "2020-06-13 14:44:30",
            "updated_at": "2020-06-13 14:44:30",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/12"
                }
            ]
        },
        "access_token": "eyJ0eXAiOiJKV1QiLC..."
    }
}</code></pre>
<blockquote>
<p>Example response (422, Validation Failed):</p>
</blockquote>
<pre><code class="language-json">{
    "error": {
        "name": [
            "The name field is required."
        ],
        "email": [
            "The email field is required."
        ],
        "password": [
            "The password field is required."
        ]
    },
    "code": 422
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/login</code></strong></p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p><code><b>email</b></code>&nbsp; <small>string</small>     <br>
The email of the user.</p>
<p><code><b>password</b></code>&nbsp; <small>string</small>     <br>
The password of the user.</p>
<h2>Refresh</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Refresh token</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/refresh" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/refresh"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
     data {
         'token':'eyJ0eXAiOiJKV1QiLC...'
     }
}</code></pre>
<blockquote>
<p>Example response (401, Unauthenticated):</p>
</blockquote>
<pre><code class="language-json">
{
 "error": {
    "error": "Unauthenticated.",
    "code": 401
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/refresh</code></strong></p>
<h2>Logout</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Logout for aunthenticated users</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
     data {
         'message':'Successfully logged out'
     }
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/logout</code></strong></p>
<h2>Me</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Get the authenticated User.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
     data: {
          "resource": 3,
          "full_name": "Bernard Abshire",
          "email": "lavon.stiedemann@example.org",
          "role": "Admin",
          "creator": "System",
          "editor": "System",
          "created_at": "2020-06-12 21:44:12",
          "updated_at": "2020-06-12 21:44:12",
          "links": [
              {
                  "rel": "self",
                  "href": "http://localhost:8000/api/users/3"
              }
          ]
      }
}</code></pre>
<blockquote>
<p>Example response (401, Unauthenticated):</p>
</blockquote>
<pre><code class="language-json">
{
 "error": {
    "error": "Unauthenticated.",
    "code": 401
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/me</code></strong></p><h1>Endpoints</h1>
<h2>docs.json</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/docs.json" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/docs.json"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "variables": [],
    "info": {
        "name": "API RESTFUL TEST API",
        "_postman_id": "e20a13d8-cd84-4d6a-a771-d62db3e02283",
        "description": "",
        "schema": "https:\/\/schema.getpostman.com\/json\/collection\/v2.0.0\/collection.json"
    },
    "item": [
        {
            "name": "Auth endpoints",
            "description": "\nAuthentication methods: Register, Login, Logout, Refresh Token and Me",
            "item": [
                {
                    "name": "Registration",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/register",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"name\": \"et\",\n    \"email\": \"sequi\",\n    \"password\": \"sint\",\n    \"password_confirmation\": \"aliquid\"\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Creates a user with role \"User\" (It doesnt allow to chose a role)",
                        "response": []
                    }
                },
                {
                    "name": "Login",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/login",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"email\": \"mariano.paz.flores@gmail.com\",\n    \"password\": \"pariatur\"\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Login Method to allow users to use all available endpoints through an access token",
                        "response": []
                    }
                },
                {
                    "name": "Refresh",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/refresh",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Refresh token",
                        "response": []
                    }
                },
                {
                    "name": "Logout",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/logout",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Logout for aunthenticated users",
                        "response": []
                    }
                },
                {
                    "name": "Me",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/me",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Get the authenticated User.",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Endpoints",
            "description": "",
            "item": [
                {
                    "name": "docs.json",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "docs.json",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Send a reset link to the given user.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/password\/email",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Reset the given user's password.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/password\/reset",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Resend the email verification notification.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/email\/resend",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Mark the authenticated user's email address as verified.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/email\/verify\/:id\/:hash",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Display a listing of the resource.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Store a newly created resource in storage.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Display the specified resource.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Update the specified resource in storage.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user",
                            "query": []
                        },
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                },
                {
                    "name": "Remove the specified resource from storage.",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user",
                            "query": []
                        },
                        "method": "DELETE",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Likes endpoints",
            "description": "\nMovie methods available:\n\nFor Regular Users and Admins: Store",
            "item": [
                {
                    "name": "Likes",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user\/movies\/:movie\/likes",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "User likes\/Unlike a movie",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Movie endpoints",
            "description": "\nMovie methods available:\n\nFor Regular Users, Admin and Guests: Index, Show\nFor Admin Users only: Store, Update and Destroy",
            "item": [
                {
                    "name": "Listing Movies",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies",
                            "query": [
                                {
                                    "key": "sortBy",
                                    "value": "dolorum",
                                    "description": "to sort by. Defaults to 'name'.",
                                    "disabled": false
                                },
                                {
                                    "key": "sortByDesc",
                                    "value": "ad",
                                    "description": "to sort by desc",
                                    "disabled": false
                                },
                                {
                                    "key": "per_page",
                                    "value": "mollitia",
                                    "description": "to paginate the listing. Default to 15",
                                    "disabled": false
                                },
                                {
                                    "key": "filters:",
                                    "value": "name%3D%22Autem%22",
                                    "description": "add the name of the field follow by colon and the value.",
                                    "disabled": false
                                }
                            ]
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Display a listing of movies with pagination, filters, searching, etc.",
                        "response": []
                    }
                },
                {
                    "name": "Create Movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "multipart\/form-data"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "formdata",
                            "formdata": [
                                {
                                    "key": "title",
                                    "value": "vel",
                                    "type": "text"
                                },
                                {
                                    "key": "description",
                                    "value": "hic",
                                    "type": "text"
                                },
                                {
                                    "key": "available",
                                    "value": "unavailable",
                                    "type": "text"
                                },
                                {
                                    "key": "rental_price",
                                    "value": 1726994.38,
                                    "type": "text"
                                },
                                {
                                    "key": "sale_price",
                                    "value": 46.01566,
                                    "type": "text"
                                },
                                {
                                    "key": "daily_penalty",
                                    "value": 710977346.34,
                                    "type": "text"
                                },
                                {
                                    "key": "poster",
                                    "src": [],
                                    "type": "file"
                                }
                            ]
                        },
                        "description": "Store a newly created movie in storage. (Only admin users)",
                        "response": []
                    }
                },
                {
                    "name": "Show Movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies\/:movie",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Display the specified movie.",
                        "response": []
                    }
                },
                {
                    "name": "Update Movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies\/:movie",
                            "query": []
                        },
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "multipart\/form-data"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "formdata",
                            "formdata": [
                                {
                                    "key": "name",
                                    "value": "Titanic",
                                    "type": "text"
                                },
                                {
                                    "key": "synopsis",
                                    "value": "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
                                    "type": "text"
                                },
                                {
                                    "key": "status",
                                    "value": "Available\nThe value must be one of available, unavailable, Available, or Unavailable.",
                                    "type": "text"
                                },
                                {
                                    "key": "rent_daily",
                                    "value": "1.00",
                                    "type": "text"
                                },
                                {
                                    "key": "price_sale",
                                    "value": "2.5",
                                    "type": "text"
                                },
                                {
                                    "key": "penalty_per_day",
                                    "value": "1.25",
                                    "type": "text"
                                },
                                {
                                    "key": "poster",
                                    "src": [],
                                    "type": "file"
                                }
                            ]
                        },
                        "description": "Update the specified movie in storage. You need to at least update 1 field. (Only admin users)",
                        "response": []
                    }
                },
                {
                    "name": "Delete a movie",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/movies\/:movie",
                            "query": []
                        },
                        "method": "DELETE",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Remove the specified movie from storage (Only admin users)",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Rentals(Transaction) endpoints",
            "description": "\nMovie methods available:\n\nFor Regular Users, Admin and Guests: Show, Update\nFor Admin Users only: Index,",
            "item": [
                {
                    "name": "Listing rentals",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/rentals",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "List of all transactions available (Only admin users)",
                        "response": []
                    }
                },
                {
                    "name": "Show Rental",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/rentals\/:rental",
                            "query": []
                        },
                        "method": "GET",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "[]",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Display the specified transaction.",
                        "response": []
                    }
                },
                {
                    "name": "Update Rental",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/rentals\/:rental",
                            "query": []
                        },
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"qty\": 4286941.09093,\n    \"transaction_type\": \"purchase\",\n    \"return_date\": \"2020-06-13 16:21:18\",\n    \"transaction\": \"Rent\"\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "Update specified transaction.",
                        "response": []
                    }
                }
            ]
        },
        {
            "name": "Transaction endpoint",
            "description": "",
            "item": [
                {
                    "name": "Create a Rental\/Transaction",
                    "request": {
                        "url": {
                            "protocol": "http",
                            "host": "localhost",
                            "path": "api\/users\/:user\/movies\/:movie\/rentals",
                            "query": []
                        },
                        "method": "POST",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application\/json"
                            },
                            {
                                "key": "Accept",
                                "value": "application\/json"
                            },
                            {
                                "key": "Authorization:",
                                "value": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"transaction\": \"Rent\",\n    \"qty\": 283212.41706421\n}",
                            "options": {
                                "raw": {
                                    "language": "json"
                                }
                            }
                        },
                        "description": "It generates a rental\/purchase for an especific user",
                        "response": []
                    }
                }
            ]
        }
    ]
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>docs.json</code></strong></p>
<h2>Send a reset link to the given user.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/password/email" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/password/email"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/password/email</code></strong></p>
<h2>Reset the given user&#039;s password.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/password/reset</code></strong></p>
<h2>Resend the email verification notification.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/email/resend" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/email/resend"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Unauthenticated.",
    "code": 401
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/email/resend</code></strong></p>
<h2>Mark the authenticated user&#039;s email address as verified.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/email/verify/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/email/verify/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (403):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Forbidden",
    "code": 403
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/email/verify/{id}/{hash}</code></strong></p>
<h2>Display a listing of the resource.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Unauthenticated.",
    "code": 401
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/users</code></strong></p>
<h2>Store a newly created resource in storage.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/users</code></strong></p>
<h2>Display the specified resource.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (401):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Unauthenticated.",
    "code": 401
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/users/{user}</code></strong></p>
<h2>Update the specified resource in storage.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>Request</h3>
<p><small class="badge badge-darkblue">PUT</small>
<strong><code>api/users/{user}</code></strong></p>
<p><small class="badge badge-purple">PATCH</small>
<strong><code>api/users/{user}</code></strong></p>
<h2>Remove the specified resource from storage.</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://localhost/api/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>Request</h3>
<p><small class="badge badge-red">DELETE</small>
<strong><code>api/users/{user}</code></strong></p><h1>Likes endpoints</h1>
<p>Movie methods available:</p>
<p>For Regular Users and Admins: Store</p>
<h2>Likes</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>User likes/Unlike a movie</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/users/1/movies/1/likes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users/1/movies/1/likes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "resource": 3,
        "user": "Bernard Abshire",
        "role": "Admin",
        "creator": "System",
        "editor": "System",
        "created_at": "2020-06-12 21:44:12",
        "updated_at": "2020-06-12 21:44:12",
        "likes": [
            {
                "resource": 1,
                "name": "Excepturi ut exercitationem eos deserunt quisquam.",
                "synopsis": "Aut asperiores omnis possimus velit quisquam error. Deleniti debitis asperiores quia possimus aut. Exercitationem inventore eligendi quia repudiandae. Omnis sit qui quas voluptatum. Consequatur molestiae reprehenderit non aliquam. Debitis ratione assumenda id nulla possimus. Minus voluptas enim odio temporibus quod illo laudantium.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.7,
                "price_sale": 9.3,
                "penalty_per_day": 1.1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/1"
                    }
                ]
            },
            {
                "resource": 12,
                "name": "Et rerum quaerat omnis ab est vero.",
                "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.6,
                "price_sale": 6.1,
                "penalty_per_day": 1.5,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/12"
                    }
                ]
            },
            {
                "resource": 26,
                "name": "Sit totam officia debitis.",
                "synopsis": "Quidem itaque quae qui nemo quis voluptas minus accusamus. Ad est et similique rerum consectetur. Qui ducimus quisquam inventore blanditiis odit laborum ipsa. At aliquam numquam rerum enim voluptas.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Available",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.5,
                "price_sale": 8.6,
                "penalty_per_day": 1.2,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/26"
                    }
                ]
            },
            {
                "resource": 33,
                "name": "Autem placeat voluptatibus fugit.",
                "synopsis": "Ipsum ullam dignissimos incidunt. Eligendi a consequuntur ipsa illum et quam vero. Voluptatibus animi sed veritatis repellat at repellendus dignissimos voluptas. Error corrupti sed est corporis esse.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 2,
                "status": "Available",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.8,
                "price_sale": 7.6,
                "penalty_per_day": 1.5,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/33"
                    }
                ]
            },
            {
                "resource": 39,
                "name": "Possimus unde cumque esse saepe nulla vel.",
                "synopsis": "Alias reprehenderit iste accusantium qui molestiae dolorem reprehenderit. Et ea et voluptas fugit molestiae molestiae numquam. Omnis neque est iusto labore. Nemo ratione aliquam sunt. Soluta enim ea temporibus et labore numquam saepe necessitatibus. Magnam molestiae aperiam ut aut. Itaque quis non molestiae maiores earum in.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 1,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.6,
                "price_sale": 8.5,
                "penalty_per_day": 1.4,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/39"
                    }
                ]
            },
            {
                "resource": 50,
                "name": "Mariano Paz",
                "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 2,
                "status": "Unavailable",
                "creator": "System",
                "editor": "Ramona Barrows",
                "rent_daily": 4.8,
                "price_sale": 2.2,
                "penalty_per_day": 1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:57:35",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/50"
                    }
                ]
            }
        ],
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/users\/3"
            }
        ]
    }
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/users/{user}/movies/{movie}/likes</code></strong></p><h1>Movie endpoints</h1>
<p>Movie methods available:</p>
<p>For Regular Users, Admin and Guests: Index, Show
For Admin Users only: Store, Update and Destroy</p>
<h2>Listing Movies</h2>
<p>Display a listing of movies with pagination, filters, searching, etc.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/movies?sortBy=porro&amp;sortByDesc=nisi&amp;per_page=nihil&amp;filters%3A=name%3D%22Autem%22" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/movies"
);

let params = {
    "sortBy": "porro",
    "sortByDesc": "nisi",
    "per_page": "nihil",
    "filters:": "name="Autem"",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
     "data": {
          {
              "resource": 33,
              "name": "Autem placeat voluptatibus fugit.",
              "synopsis": "Ipsum ullam dignissimos incidunt. Eligendi a consequuntur ipsa illum et quam vero. Voluptatibus animi sed veritatis repellat at repellendus dignissimos voluptas. Error corrupti sed est corporis esse.",
              "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
              "likes": 2,
              "rent_daily": 1.8,
              "price_sale": 7.6,
              "penalty_per_day": 1.5,
              "created_at": "2020-06-12 21:44:12",
              "updated_at": "2020-06-12 21:44:12",
              "links": [
                  {
                      "rel": "self",
                      "href": "http:\/\/localhost\/api\/movies\/33"
                  }
              ]
          },
          {
              "resource": 22,
              "name": "Debitis dolore totam labore omnis asperiores dolore necessitatibus.",
              "synopsis": "Nulla eos doloremque rerum maiores. Veniam qui repellendus deleniti nemo provident. Quia quod soluta blanditiis nihil quos et. Vero voluptas officia voluptatum consequatur. Et veniam doloribus quia aut. Et quis explicabo voluptatibus accusamus voluptatem est.",
              "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
              "likes": 0,
              "rent_daily": 1.8,
              "price_sale": 7.1,
              "penalty_per_day": 1.4,
              "created_at": "2020-06-12 21:44:12",
              "updated_at": "2020-06-12 21:44:12",
              "links": [
                  {
                      "rel": "self",
                      "href": "http:\/\/localhost\/api\/movies\/22"
                  }
              ]
          },
          {
              "resource": 10,
              "name": "Distinctio eaque fugit assumenda illum.",
              "synopsis": "Doloribus et assumenda excepturi minima totam odio omnis. Ea numquam tempore dicta facere. Quos modi distinctio fugit quasi et consequatur. Qui sunt exercitationem quisquam eum est. Ut suscipit eum dolor in rerum laborum est. Doloremque sed atque harum iste at. Ea eum occaecati at.",
              "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
              "likes": 0,
              "rent_daily": 1.3,
              "price_sale": 5.5,
              "penalty_per_day": 1,
              "created_at": "2020-06-12 21:44:12",
              "updated_at": "2020-06-12 21:44:12",
              "links": [
                  {
                      "rel": "self",
                      "href": "http:\/\/localhost\/api\/movies\/10"
                  }
              ]
          },
     },
     "pagination": {
         "current_page": 1,
         "from": 1,
         "last_page": 2,
         "per_page": 15,
         "to": 15,
         "total": 21,
         "count": 15,
         "prev": null,
         "next": "http:\/\/localhost\/api\/movies?page=2",
         "first": "http:\/\/localhost\/api\/movies?page=1",
         "last": "http:\/\/localhost\/api\/movies?page=2"
     }
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/movies</code></strong></p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p><code><b>sortBy</b></code>&nbsp;          <i>optional</i>    <br>
to sort by. Defaults to 'name'.</p>
<p><code><b>sortByDesc</b></code>&nbsp;          <i>optional</i>    <br>
to sort by desc</p>
<p><code><b>per_page</b></code>&nbsp;          <i>optional</i>    <br>
to paginate the listing. Default to 15</p>
<p><code><b>filters:</b></code>&nbsp;          <i>optional</i>    <br>
add the name of the field follow by colon and the value.</p>
<h2>Create Movie</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Store a newly created movie in storage. (Only admin users)</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/movies" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -F "title=accusamus" \
    -F "description=quis" \
    -F "available=Unavailable" \
    -F "rental_price=71965774.142753" \
    -F "sale_price=2811784.549586" \
    -F "daily_penalty=1.21608322" \
    -F "poster=@C:\Users\squeaky\AppData\Local\Temp\phpC75D.tmp" </code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/movies"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

const body = new FormData();
body.append('title', 'accusamus');
body.append('description', 'quis');
body.append('available', 'Unavailable');
body.append('rental_price', '71965774.142753');
body.append('sale_price', '2811784.549586');
body.append('daily_penalty', '1.21608322');
body.append('poster', document.querySelector('input[name="poster"]').files[0]);

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
"data": {
  "resource": 82,
  "name": "New Movie 2020",
  "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
  "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
  "likes": 0,
  "status": "Unavailable",
  "creator": "Bernard Abshire",
  "editor": "Bernard Abshire",
  "rent_daily": 1.25,
  "price_sale": 15.45,
  "penalty_per_day": 1,
  "created_at": "2020-06-13 15:14:34",
  "updated_at": "2020-06-13 15:14:34",
  "links": [
      {
          "rel": "self",
          "href": "http://localhost:8000/api/movies/82"
      }
  ]
}</code></pre>
<blockquote>
<p>Example response (422, Validation Failed):</p>
</blockquote>
<pre><code class="language-json">{
    "error": {
        "name": [
            "The name has already been taken."
        ]
    },
    "code": 422
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/movies</code></strong></p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p><code><b>title</b></code>&nbsp; <small>string</small>     <br></p>
<p><code><b>poster</b></code>&nbsp; <small>file</small>         <i>optional</i>    <br>
The value must be an image.</p>
<p><code><b>description</b></code>&nbsp; <small>string</small>     <br></p>
<p><code><b>available</b></code>&nbsp; <small>string</small>     <br>
The value must be one of <code>available</code>, <code>unavailable</code>, <code>Available</code>, or <code>Unavailable</code>.</p>
<p><code><b>rental_price</b></code>&nbsp; <small>number</small>     <br></p>
<p><code><b>sale_price</b></code>&nbsp; <small>number</small>     <br></p>
<p><code><b>daily_penalty</b></code>&nbsp; <small>number</small>     <br></p>
<h2>Show Movie</h2>
<p>Display the specified movie.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/movies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/movies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (404, Not Found):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Does not exist any instance of movie with the given id",
    "code": 404
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "resource": 1,
        "name": "Excepturi ut exercitationem eos deserunt quisquam.",
        "synopsis": "Aut asperiores omnis possimus velit quisquam error. Deleniti debitis asperiores quia possimus aut. Exercitationem inventore eligendi quia repudiandae. Omnis sit qui quas voluptatum. Consequatur molestiae reprehenderit non aliquam. Debitis ratione assumenda id nulla possimus. Minus voluptas enim odio temporibus quod illo laudantium.",
        "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
        "likes": 1,
        "rent_daily": 1.7,
        "price_sale": 9.3,
        "penalty_per_day": 1.1,
        "created_at": "2020-06-12 21:44:12",
        "updated_at": "2020-06-12 21:44:12",
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost\/api\/movies\/1"
            }
        ]
    }
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/movies/{movie}</code></strong></p>
<h2>Update Movie</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Update the specified movie in storage. You need to at least update 1 field. (Only admin users)</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/movies/1" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -F "name=Titanic" \
    -F "synopsis=Lorem ipsum dolor sit, amet consectetur adipisicing elit." \
    -F "status=Available
The value must be one of available, unavailable, Available, or Unavailable." \
    -F "rent_daily=1.00" \
    -F "price_sale=2.5" \
    -F "penalty_per_day=1.25" \
    -F "poster=@C:\Users\squeaky\AppData\Local\Temp\phpC79C.tmp" </code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/movies/1"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

const body = new FormData();
body.append('name', 'Titanic');
body.append('synopsis', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.');
body.append('status', 'Available
The value must be one of available, unavailable, Available, or Unavailable.');
body.append('rent_daily', '1.00');
body.append('price_sale', '2.5');
body.append('penalty_per_day', '1.25');
body.append('poster', document.querySelector('input[name="poster"]').files[0]);

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
"data": {
  "resource": 82,
  "name": "New Movie 2020",
  "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
  "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
  "likes": 0,
  "status": "Unavailable",
  "creator": "Bernard Abshire",
  "editor": "Bernard Abshire",
  "rent_daily": 1.25,
  "price_sale": 15.45,
  "penalty_per_day": 1,
  "created_at": "2020-06-13 15:14:34",
  "updated_at": "2020-06-13 15:14:34",
  "links": [
      {
          "rel": "self",
          "href": "http://localhost:8000/api/movies/82"
      }
  ]
}</code></pre>
<blockquote>
<p>Example response (422, Validation Failed):</p>
</blockquote>
<pre><code class="language-json">{
    "error": {
        "name": [
            "The name has already been taken."
        ]
    },
    "code": 422
}</code></pre>
<blockquote>
<p>Example response (404, Not Found):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Does not exist any instance of movie with the given id",
    "code": 404
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-darkblue">PUT</small>
<strong><code>api/movies/{movie}</code></strong></p>
<p><small class="badge badge-purple">PATCH</small>
<strong><code>api/movies/{movie}</code></strong></p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p><code><b>name</b></code>&nbsp; <small>string.</small>         <i>optional</i>    <br></p>
<p><code><b>poster</b></code>&nbsp; <small>file</small>         <i>optional</i>    <br>
The value must be an image.</p>
<p><code><b>synopsis</b></code>&nbsp; <small>string.</small>         <i>optional</i>    <br></p>
<p><code><b>status</b></code>&nbsp; <small>string.</small>         <i>optional</i>    <br></p>
<p><code><b>rent_daily</b></code>&nbsp; <small>number.</small>         <i>optional</i>    <br></p>
<p><code><b>price_sale</b></code>&nbsp; <small>number.</small>         <i>optional</i>    <br></p>
<p><code><b>penalty_per_day</b></code>&nbsp; <small>number.</small>         <i>optional</i>    <br></p>
<h2>Delete a movie</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Remove the specified movie from storage (Only admin users)</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://localhost/api/movies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/movies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">
{
"data": {
  "resource": 82,
  "name": "New Movie 2020",
  "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
  "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
  "likes": 0,
  "status": "Unavailable",
  "creator": "Bernard Abshire",
  "editor": "Bernard Abshire",
  "rent_daily": 1.25,
  "price_sale": 15.45,
  "penalty_per_day": 1,
  "created_at": "2020-06-13 15:14:34",
  "updated_at": "2020-06-13 15:14:34",
  "links": [
      {
          "rel": "self",
          "href": "http://localhost:8000/api/movies/82"
      }
  ]
}</code></pre>
<blockquote>
<p>Example response (404, Not Found):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Does not exist any instance of movie with the given id",
    "code": 404
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-red">DELETE</small>
<strong><code>api/movies/{movie}</code></strong></p><h1>Rentals(Transaction) endpoints</h1>
<p>Movie methods available:</p>
<p>For Regular Users, Admin and Guests: Show, Update
For Admin Users only: Index,</p>
<h2>Listing rentals</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>List of all transactions available (Only admin users)</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/rentals" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/rentals"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "resource": 1,
            "transaction": "RENT",
            "qty": 1,
            "creator": "Ramona Barrows",
            "editor": "Ramona Barrows",
            "rent_date": "2020-06-12 21:44:17",
            "due_date": "2020-06-15 21:44:17",
            "return_date": "2020-06-16 04:00:15",
            "daily_penalty": 1,
            "rental_price": 1.2,
            "movie": {
                "resource": 50,
                "name": "Mariano Paz",
                "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 0,
                "status": "Unavailable",
                "creator": "System",
                "editor": "Ramona Barrows",
                "rent_daily": 4.8,
                "price_sale": 2.2,
                "penalty_per_day": 1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:57:35",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/50"
                    }
                ]
            },
            "customer": {
                "resource": 5,
                "full_name": "Mr. Sylvester Murphy V",
                "email": "mcclure.ross@example.org",
                "role": "User",
                "creator": "System",
                "editor": "System",
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/users\/5"
                    }
                ]
            },
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/rentals\/1"
                }
            ]
        },
        {
            "resource": 2,
            "transaction": "PURCHASE",
            "qty": 2,
            "creator": "Bernard Abshire",
            "editor": "Bernard Abshire",
            "sale_price": 2.2,
            "movie": {
                "resource": 50,
                "name": "Mariano Paz",
                "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 0,
                "status": "Unavailable",
                "creator": "System",
                "editor": "Ramona Barrows",
                "rent_daily": 4.8,
                "price_sale": 2.2,
                "penalty_per_day": 1,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:57:35",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/50"
                    }
                ]
            },
            "customer": {
                "resource": 3,
                "full_name": "Bernard Abshire",
                "email": "lavon.stiedemann@example.org",
                "role": "Admin",
                "creator": "System",
                "editor": "System",
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/users\/3"
                    }
                ]
            },
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/rentals\/2"
                }
            ]
        },
        {
            "resource": 3,
            "transaction": "RENT",
            "qty": 1,
            "creator": "Bernard Abshire",
            "editor": "Bernard Abshire",
            "rent_date": "2020-06-13 02:20:15",
            "due_date": "2020-06-16 02:20:15",
            "return_date": "2020-06-18 04:00:12",
            "daily_penalty": 1.5,
            "rental_price": 1.6,
            "movie": {
                "resource": 12,
                "name": "Et rerum quaerat omnis ab est vero.",
                "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
                "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
                "likes": 0,
                "status": "Unavailable",
                "creator": "System",
                "editor": "System",
                "rent_daily": 1.6,
                "price_sale": 6.1,
                "penalty_per_day": 1.5,
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/movies\/12"
                    }
                ]
            },
            "customer": {
                "resource": 3,
                "full_name": "Bernard Abshire",
                "email": "lavon.stiedemann@example.org",
                "role": "Admin",
                "creator": "System",
                "editor": "System",
                "created_at": "2020-06-12 21:44:12",
                "updated_at": "2020-06-12 21:44:12",
                "links": [
                    {
                        "rel": "self",
                        "href": "http:\/\/localhost:8000\/api\/users\/3"
                    }
                ]
            },
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/rentals\/3"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/localhost:8000\/api\/rentals?page=1",
        "last": "http:\/\/localhost:8000\/api\/rentals?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost:8000\/api\/rentals",
        "per_page": 15,
        "to": 3,
        "total": 3
    }
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/rentals</code></strong></p>
<h2>Show Rental</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Display the specified transaction.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/rentals/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/rentals/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "resource": 3,
        "transaction": "RENT",
        "qty": 1,
        "creator": "Bernard Abshire",
        "editor": "Bernard Abshire",
        "rent_date": "2020-06-13 02:20:15",
        "due_date": "2020-06-16 02:20:15",
        "return_date": "2020-06-18 04:00:12",
        "daily_penalty": 1.5,
        "rental_price": 1.6,
        "movie": {
            "resource": 12,
            "name": "Et rerum quaerat omnis ab est vero.",
            "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
            "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
            "likes": 0,
            "status": "Unavailable",
            "creator": "System",
            "editor": "System",
            "rent_daily": 1.6,
            "price_sale": 6.1,
            "penalty_per_day": 1.5,
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/movies\/12"
                }
            ]
        },
        "customer": {
            "resource": 3,
            "full_name": "Bernard Abshire",
            "email": "lavon.stiedemann@example.org",
            "role": "Admin",
            "creator": "System",
            "editor": "System",
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/3"
                }
            ]
        },
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/rentals\/3"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (404, Not Found):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Does not exist any instance of rentals with the given id",
    "code": 404
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-green">GET</small>
<strong><code>api/rentals/{rental}</code></strong></p>
<h2>Update Rental</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Update specified transaction.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://localhost/api/rentals/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -d '{"transaction":"Rent","qty":351456061.82198256}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/rentals/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

let body = {
    "transaction": "Rent",
    "qty": 351456061.82198256
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "resource": 2,
        "transaction": "PURCHASE",
        "qty": 2,
        "creator": "Bernard Abshire",
        "editor": "Bernard Abshire",
        "sale_price": 2.2,
        "movie": {
            "resource": 50,
            "name": "Mariano Paz",
            "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
            "image": "http:\/\/localhost:8000\/img\/film-poster-placeholder.png",
            "likes": 0,
            "status": "Unavailable",
            "creator": "System",
            "editor": "Ramona Barrows",
            "rent_daily": 4.8,
            "price_sale": 2.2,
            "penalty_per_day": 1,
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:57:35",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/movies\/50"
                }
            ]
        },
        "customer": {
            "resource": 3,
            "full_name": "Bernard Abshire",
            "email": "lavon.stiedemann@example.org",
            "role": "Admin",
            "creator": "System",
            "editor": "System",
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/3"
                }
            ]
        },
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/rentals\/2"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (404, Not Found):</p>
</blockquote>
<pre><code class="language-json">{
    "error": "Does not exist any instance of rentals with the given id",
    "code": 404
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-darkblue">PUT</small>
<strong><code>api/rentals/{rental}</code></strong></p>
<p><small class="badge badge-purple">PATCH</small>
<strong><code>api/rentals/{rental}</code></strong></p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p><code><b>transaction</b></code>&nbsp; <small>string</small>         <i>optional</i>    <br>
optional Type of transaction value in <code>rent</code>, <code>RENT</code>, <code>Rent</code>,<code>Purchase</code>, <code>PURCHASE</code>, <code>purchase</code>.</p>
<p><code><b>qty</b></code>&nbsp; <small>number</small>         <i>optional</i>    <br>
optional quantity of movies for purchasing or renting.</p><h1>Transaction endpoint</h1>
<h2>Create a Rental/Transaction</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>It generates a rental/purchase for an especific user</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/users/1/movies/1/rentals" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization:: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..." \
    -d '{"transaction":"Rent","qty":262.68}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/users/1/movies/1/rentals"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization:": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
};

let body = {
    "transaction": "Rent",
    "qty": 262.68
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "resource": 4,
        "transaction": "RENT",
        "qty": 1,
        "creator": "Bernard Abshire",
        "editor": "Bernard Abshire",
        "rent_date": "2020-06-13T16:05:20.823672Z",
        "due_date": "2020-06-16T16:05:20.823672Z",
        "return_date": "",
        "daily_penalty": 1,
        "rental_price": 1.25,
        "movie": {
            "resource": 82,
            "name": "New Movie 2020",
            "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
            "image": "http:\/\/localhost:8000\/img\/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
            "likes": 0,
            "status": "Unavailable",
            "creator": "Bernard Abshire",
            "editor": "Bernard Abshire",
            "rent_daily": 1.25,
            "price_sale": 15.45,
            "penalty_per_day": 1,
            "created_at": "2020-06-13 15:14:34",
            "updated_at": "2020-06-13 15:14:34",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/movies\/82"
                }
            ]
        },
        "customer": {
            "resource": 3,
            "full_name": "Bernard Abshire",
            "email": "lavon.stiedemann@example.org",
            "role": "Admin",
            "creator": "System",
            "editor": "System",
            "created_at": "2020-06-12 21:44:12",
            "updated_at": "2020-06-12 21:44:12",
            "links": [
                {
                    "rel": "self",
                    "href": "http:\/\/localhost:8000\/api\/users\/3"
                }
            ]
        },
        "links": [
            {
                "rel": "self",
                "href": "http:\/\/localhost:8000\/api\/rentals\/4"
            }
        ]
    }
}</code></pre>
<h3>Request</h3>
<p><small class="badge badge-black">POST</small>
<strong><code>api/users/{user}/movies/{movie}/rentals</code></strong></p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p><code><b>transaction</b></code>&nbsp; <small>string</small>     <br>
Type of transaction value in <code>rent</code>, <code>RENT</code>, <code>Rent</code>,<code>Purchase</code>, <code>PURCHASE</code>, <code>purchase</code>.</p>
<p><code><b>qty</b></code>&nbsp; <small>number</small>     <br>
quantity of movies for purchasing or renting.</p>
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var languages = ["bash","javascript"];
        setupLanguages(languages);
    });
</script>
</body>
</html>
