<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Movie;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MovieTest extends TestCase
{

    use WithFaker, DatabaseTransactions;


    public function testGetMovieList()
    {
        $this->withoutExceptionHandling();
        $response = $this->getJson('/api/movies');

        $response->assertStatus(200)
                ->assertJsonStructure([
                    'data' => ['*' => ['resource','name','synopsis','image','likes','rent_daily','price_sale','penalty_per_day','created_at','updated_at']],
                    'pagination'
                ]);
    }


    public function testCanShowMovie()
    {
        $movie  = factory(Movie::class)->create();

        $response = $this->getJson(route("movies.show", $movie->id));

        $response->assertStatus(200)
                ->assertJsonStructure([
                    'data' => [
                        'resource',
                        'name',
                        'synopsis',
                        'image',
                        'likes',
                        'rent_daily',
                        'price_sale',
                        'penalty_per_day',
                        'created_at',
                        'updated_at',
                    ]
                ]);
    }

    public function testCanCreateMovie()
    {

        $file = UploadedFile::fake()->image('avatar.jpg');

        $user     = User::where("role_id", 1)->first();
        $token    = auth()->login($user);
        $status   = ["available", "unavailable"];
        $formData = [
            'name'            => $this->faker->unique()->sentence($nbWords = 6, $variableNbWords = true),
            'image'           => $file,
            'synopsis'        => $this->faker->text(350),
            'status'          => $status[rand(0,1)],
            'rent_daily'      => rand(10, 20) / 10,
            'price_sale'      => rand(50, 100) / 10,
            'penalty_per_day' => rand(10, 15) / 10,
        ];

         //For Debugging
        $this->withoutExceptionHandling();

        $response = $this->json('POST',route('movies.store'), $formData,[
            'Authorization' => 'Bearer '.$token
        ]);

        $response
                ->assertStatus(Response::HTTP_CREATED);

    }

    public function testCanUpdateMovie()
    {
        $movie = factory(Movie::class)->create();
        $file  = UploadedFile::fake()->image('avatar.jpg');
        $user  = User::where("role_id", 1)->first();
        $token = auth()->login($user);

        $response = $this->json('PATCH',route('movies.update', $movie->id),
        [
            'name'            => $this->faker->unique()->sentence($nbWords = 6, $variableNbWords = true),
            'image'           => $file,
            'synopsis'        => $this->faker->text(350),
        ],[
            'Authorization' => 'Bearer '.$token
        ]);


        $response->assertStatus(Response::HTTP_OK);

    }

    public function testCanDeleteMovie()
    {
        $user  = User::where("role_id", 1)->first();
        $token = auth()->login($user);
        $movie = factory(Movie::class)->create();
        //For Debugging
        $this->withoutExceptionHandling();
        $this->json('DELETE',route('movies.destroy', $movie->id),[],[
            'Authorization' => 'Bearer '.$token
        ])
                ->assertStatus(Response::HTTP_OK)
                ->assertJsonStructure([
                    'data' => [
                        'resource',
                        'name',
                        'synopsis',
                        'image',
                        'likes',
                        'rent_daily',
                        'price_sale',
                        'penalty_per_day',
                        'created_at',
                        'updated_at',
                    ]
                ]);
   }
}
