<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{

    use WithFaker, DatabaseTransactions;

    public function testCanRegister()
    {
        $formData = [
            "name"                  => $this->faker->firstName().' '.$this->faker->lastName(),
            "email"                 => $this->faker->unique()->email,
            'password'              => "password",
            'password_confirmation' => "password"
        ];

         //For Debugging
        $this->withoutExceptionHandling();

        $response = $this->json('POST',route('register'), $formData);

        $response
                ->assertStatus(Response::HTTP_CREATED)
                ->assertJsonStructure([
                    'data' => ['user', 'access_token']
                ]);
    }


    public function testCanLogin()
    {
        $user = factory(User::class)->create();

        //For Debugging
        //$this->withoutExceptionHandling();

        $response = $this->actingAs($user,'api')
                    ->json('POST',route('login'),[
                        'email'    => $user->email,
                        'password' => 'password', //By default in User's factory the password is "password"
                    ]);

       $response
               ->assertStatus(Response::HTTP_OK)
               ->assertJsonStructure([
                   'data' => ['user', 'access_token']
               ]);

    }


    public function testCanLogout()
    {
        $user  = factory(User::class)->create();
        $token = auth()->login($user);

         //For Debugging
        //$this->withoutExceptionHandling();

        $this->actingAs($user,'api')
                    ->json('GET',route('auth.logout'), [],[
                        'Authorization' => 'Bearer '.$token
                    ])
                    ->assertStatus(Response::HTTP_OK)
                    ->assertJsonStructure([
                        'data' => ['message']
                    ])
                    ->assertJson([
                        'data' => [
                            'message' => 'Successfully logged out',
                        ]
                    ]);
    }

    public function testCanForgotPassword()
    {
        $user  = factory(User::class)->create();

        //For Debugging
        $this->withoutExceptionHandling();

        $this->actingAs($user,'api')
                    ->json('POST',route('forgotPassword'), [
                        'email' => $user->email
                    ])
                    ->assertStatus(Response::HTTP_OK)
                    ->assertJsonStructure([
                        'data' => ['message']
                    ])
                    ->assertJson([
                        'data' => [
                            'message' => 'We have emailed your password reset link!',
                        ]
                    ]);

    }

    public function testCanRefreshToken()
    {
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        //For Debugging
        //$this->withoutExceptionHandling();

        $response = $this->actingAs($user,'api')
                    ->json('GET',route('auth.refresh'),[],[
                        'Authorization' => 'Bearer '.$token
                    ]);


       $response
               ->assertStatus(Response::HTTP_OK)
               ->assertJsonStructure([
                   'data' => ['token']
               ]);
    }


}
