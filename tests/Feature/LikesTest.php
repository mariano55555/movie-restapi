<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Movie;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LikesTest extends TestCase
{

    use WithFaker, DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanLike()
    {
        $user  = factory(User::class)->create();
        $movie = factory(Movie::class)->create();
        $token = auth()->login($user);

        $response = $this->json('POST', route('users.movies.likes.store',[$user->id, $movie->id]),[],[
                                    'Authorization' => 'Bearer '.$token
                                ]);

        $response->assertStatus(200)
                    ->assertJsonStructure([
                        'data' => [
                            'resource',
                            'created_at',
                            'updated_at',
                            'likes',
                            'links',
                        ]
                    ]);
    }
}
