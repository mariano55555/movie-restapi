<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


# Movie Restful API

Movie Restful API, is a basic resftul API application using laravel 7.

# Features!

  - Laravel API Resources with Field tranfromations for security matters.
  - Gates and policies
  - JWT for authentication
  - Laravel Form Request for Validation
  - Flexible movie pagination: filter by likes and title (and any other field)
  - Mailtrap.io for verify email accounts
  - Custom Headers for the Aplication Name
  - HATEOAS
  - Feature Tests
  - Log table (audits)
  - Factories, Migrations and Seeders
  - etc.

### Installation

Movie Api requires:
- [Composer](https://getcomposer.org/)
- [PHP7](https://php.net/)
- [MySQL](https://www.mysql.com/) or any other SQL Database
- [Laravel7](https://laravel.com/)

1. Clone the repository
2. Install the dependencies.

```sh
$ cd movies
$ composer install
```

3. Setup your env file with your database configuration and run the migration

```sh
$ php artisan migrate --seed
```
4. Generate JWT AUTH TOKEN
```sh
$ php artisan jwt:secret
```

5. Create a test database (Optional)
```sh
$ touch database/test.sqlite
```

6. Run tests (optional)
```sh
$ ./vendor/bin/phpunit
```

7. Run the application (optional)
```sh
$ php artisan serve
```

### ENDPOINTS

Dillinger is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

#### Users


| Method | Action | Endpoint |Role
| ------ | ------ |------ |------ |
| GET | List all users (Allows pagination, filter, search, etc.) | [http://localhost:8000/api/users](http://localhost:8000/api/users) | Admin |
| POST | Create an user (Requires set up an email service credentials in env file) | [http://localhost:8000/api/users](http://localhost:8000/api/users) | Admin |
| GET | Display an specific user | [http://localhost:8000/api/users/{user}](http://localhost:8000/api/users/{user}) | Admin / User (Only his own account)
| PUT/PATCH | Update an user account | [http://localhost:8000/api/users/](http://localhost:8000/api/users/1) | Admin  / User (Only his own account)|
| DELETE | Delete a user account | [http://localhost:8000/api/users/{user}](http://localhost:8000/api/users/{user}) | Admin / User (Only his own account) |

#### Movies


| Method | Action | Endpoint |Role
| ------ | ------ |------ |------ |
| GET | List all movies (Allows pagination, filter, search, etc.) | [http://localhost:8000/api/movies](http://localhost:8000/api/movies) | Admin/User/Guest |
| POST | Create a movie | [http://localhost:8000/api/movies](http://localhost:8000/api/movies) | Admin |
| GET | Display an specific movie | [http://localhost:8000/api/movies/{movie}](http://localhost:8000/api/movies/{movie}) | Admin/User/Guest |
| POST | Like a movie | [http://localhost:8000/user/{user}/movies/{movie}/likes](http://localhost:8000/user/{user}/movies/{movie}/likes) | Admin / User |
| PUT/PATCH | Update an movie | [http://localhost:8000/movies/{movie}](http://localhost:8000/movies/{movie}) | Admin |
| DELETE | Delete a movie | [http://localhost:8000/movies/{movie}](http://localhost:8000/movies/{movie}) | Admin |


#### Rental (Transactions)


| Method | Action | Endpoint |Role
| ------ | ------ |------ |------ |
| GET | List all transactions | [http://localhost:8000/api/rentals](http://localhost:8000/api/rentals) | Admin |
| POST | Create a transaction (Rent or Purchase) | [http://localhost:8000/api/users/{user}/movies/{movie}/rentals](http://localhost:8000/api/users/{user}/movies/{movie}/rentals) | Admin/User |
| PUT/PATCH | Update a transaction (It could add the return date) | [http://localhost:8000/api/rentals/{rental}]([http://localhost:8000/api/rentals/{rental}) | Admin/User |
| GET | Display an specific transaction | [[http://localhost:8000/api/rentals/{rental}](http://localhost:8000/api/rentals/{rental}) | Admin / User (Only if the user is the customer)

#### Manage file publication

*Regular User Account:*
email: user@user.com
password: password

*Admin Account:*
email: admin@admin.com
password: password





#####  Special features:
  **List all movies (Allows pagination, filter, search, etc.)**

    Custom pagination number:
    To use a custom pagination number just include per_page attribute in the url (Default: 15).

    sortBy and sortByDesc:
    In order to sort the results, use the attribute sortBy equal to the name of the field; by default the sort would be by name asc. Use sortByDesc attribute to do a sort descendent sort.

    Search/Filter:
    In order to search an attribute, just add the name of the field equal to the search value.

    Putting all together:
    You can add sort, filter and per_page requests in the same url.

    Event Listeners:
    The movie and rentals models have event listeners to register logs in the audit table.



### Todo

 - Write a more realistic multi role rest api
 - Write more tests
 - Use intervention to resize images
 - Payment/Transactions and create invoices
 - Send invoices to customer's email, etc
