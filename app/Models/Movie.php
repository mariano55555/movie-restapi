<?php

namespace App\Models;

use App\Models\Audit;
use Wildside\Userstamps\Userstamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{

    const AVAILABLE      = 1;
    const UNAVAILABLE    = 0;
    const DEFAULT_POSTER = 'film-poster-placeholder.png';

    protected $table = 'movies';

    use Userstamps, SoftDeletes;


    public static function boot()
    {
        parent::boot();
        self::updating(function ($model) {
            $originalData = $model->getOriginal();
            // Log INFO to track rentals/purchases events (Only updating)
            if($model->title !== $originalData['title'] ||
               $model->rental_price !== $originalData['rental_price']  ||
               $model->sale_price !== $originalData['sale_price'])
            {
                Audit::create([
                    'auditable_type' => Movie::class,
                    'auditable_id'   => $model->id,
                    'new_value'      => json_encode($model->getOriginal()),
                    'old_value'      => json_encode($model),
                    'event'          => 'updating'
                ]);
            }
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'poster', 'description', 'available', 'rental_price', 'sale_price', 'daily_penalty'
    ];


    protected $dates = ['deleted_at'];


    public function likes()
    {
        return $this->belongsToMany(User::class, 'likes')->withTimestamps();
    }

    public function rental()
    {
        return $this->belongsToMany(User::class, 'rentals')->withTimestamps();
    }


    public function isAvailable()
    {
        return $this->available == Movie::AVAILABLE;
    }
}
