<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\PasswordResetNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * Send the password reset notification. (From Laravel Docs)
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }


    /**
     * Relations
     */


    public function likes()
    {
        return $this->belongsToMany(Movie::class, 'likes')->withTimestamps();
    }

    public function rental()
    {
        return $this->belongsToMany(Movie::class, 'rentals')->withTimestamps();
    }


    public function role()
    {
       return $this->belongsTo(Role::class);
    }


    /**
     * Custom Functions
     */


    public function hasRole($role)
    {
       if ($this->role()->where("name", $role)->first()) {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        return $this->hasRole('Admin');
    }
}
