<?php

namespace App\Models;

use Throwable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Wildside\Userstamps\Userstamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rental extends Model
{
    use Userstamps, SoftDeletes;

    const DEFAULT_RENT_DAYS = 3;
    const BUY_TRANSACTION   = 1;
    const RENT_TRANSACTION  = 0;

    protected $table = 'rentals';

    use Userstamps, SoftDeletes;


    public static function boot()
    {
        parent::boot();
        self::updating(function ($model) {

            if($model->return_date !== $model->getOriginal('return_date')){

                $dueDate = Carbon::parse($model->due_date);
                $returnDate   = Carbon::parse($model->return_date);

                $diffDays = $returnDate->gt($dueDate);
                if($diffDays){ // Daily Penalty
                    $diffDays    = $dueDate->diffInDays($returnDate);
                    $diffMinutes = $dueDate->diffInMinutes($returnDate);

                    if($diffDays > 0){
                        $comment = 'Difference in days: ' . $diffDays;
                        Log::info($comment);
                        $diff = $diffDays;
                    }else{ //If it is less than a day we charge as 1 day
                        $comment = 'Difference in minutes: ' . $diffMinutes;
                        Log::info($comment);
                        $diff = 1;
                    }

                    $model->fines()->create([
                        'comment' => $comment,
                        'fine' => (float) ($diff * $model->daily_penalty)
                    ]);
                }
            }
            // Log INFO to track rentals/purchases events (creating and updating)


                Audit::create([
                    'auditable_type' => Rental::class,
                    'auditable_id'   => $model->id,
                    'new_value'      => json_encode($model->getOriginal()),
                    'old_value'      => json_encode($model),
                    'event'          => 'updating'
                ]);

        });

        self::created(function ($model) {

                Audit::create([
                    'auditable_type' => Rental::class,
                    'auditable_id'   => $model->id,
                    'new_value'      => json_encode($model),
                    'event'          => 'creating'
                ]);

        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'movie_id', 'qty', 'transaction_type', 'rental_date', 'due_date', 'return_date', 'rental_price', 'sale_price', 'daily_penalty'
    ];


    protected $dates = ['deleted_at'];


    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id');
    }

    public function getTransactionTypeAttribute($valor)
    {
        return ( $valor == 0 ) ? "RENT" : "PURCHASE";
    }


    public function fines()
    {
        return $this->hasMany(Fine::class, 'rental_id');
    }
}
