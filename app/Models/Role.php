<?php

namespace App\Models;

use Wildside\Userstamps\Userstamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use Userstamps, SoftDeletes;

    protected $table = 'roles';

    use Userstamps, SoftDeletes;

    const ADMIN = 1;
    const USER  = 2;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];



    protected $dates = ['deleted_at'];
}
