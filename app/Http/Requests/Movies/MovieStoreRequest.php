<?php

namespace App\Http\Requests\Movies;

use App\Models\Movie;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Resources\Movies\MovieResource;

class MovieStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user() && auth()->user()->isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|max:255|unique:movies,title',
            'poster'        => 'image',
            'description'   => 'required',
            'available'     => ['required', Rule::in(['available', 'unavailable', 'Available', 'Unavailable'])],
            'rental_price'  => 'required|numeric',
            'sale_price'    => 'required|numeric',
            'daily_penalty' => 'required|numeric'
        ];
    }
}
