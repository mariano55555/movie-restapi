<?php

namespace App\Http\Requests\Rentals;

use App\Models\Rental;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RentalStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qty'              => 'required|numeric',
            'transaction_type' => ['required', Rule::in(['rent', 'RENT', 'Rent', 'Purchase', 'PURCHASE', 'purchase'])],
        ];
    }
}
