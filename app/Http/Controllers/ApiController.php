<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;

class ApiController extends Controller
{
    use ApiResponser;

    public $gate;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('verified');
    }

    protected function adminAccess(){

        if (Gate::denies('admin-access')) {
            throw new AuthorizationException("You don't have enough privileges to access this area.");
        }
    }
}
