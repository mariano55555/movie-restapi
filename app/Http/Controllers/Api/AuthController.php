<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Response;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\Auth\AuthResource;
use App\Http\Resources\User\UserResosurce;
use App\Http\Requests\Auth\RegisterRequest;
/**
 * @group Auth endpoints
 *
 * Authentication methods: Register, Login, Logout, Refresh Token and Me
 */
class AuthController extends ApiController
{

    public function __construct()
    {
        $this->middleware('auth:api')->only(['logout', 'refresh', 'me']);
        $this->middleware('verified')->only(['logout', 'refresh']);
    }

     /**
     * Registration
     *
     * Creates a user with role "User" (It doesnt allow to chose a role)
     *
     * @bodyParam name string required The name of the user.
     * @bodyParam email string required The email of the user.
     * @bodyParam password string required The password.
     * @bodyParam password_confirmation string required the confirmation of the password
     *
     *
     * @response 201 {
     *      "data": {
     *          "user": {
     *              "resource": 12,
     *              "full_name": "mariansopfaasdfa",
     *              "email": "mariaasdnoasdfa1asdfa2@marianosa.com",
     *              "created_at": "2020-06-13 14:44:30",
     *              "updated_at": "2020-06-13 14:44:30",
     *              "links": [
     *                  {
     *                      "rel": "self",
     *                      "href": "http://localhost:8000/api/users/12"
     *                  }
     *               ]
     *          },
     *          "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJA..."
     *      }
     * }
     *
     * @response status=422 scenario="Validation Failed" {
     *  "error": {
     *       "password": [
     *           "The password field is required."
     *       ]
     *   },
     *   "code": 422
     * }
     *
     *
     */
    public function register(RegisterRequest $request)
    {

        $validateData = $request->validated();
        $validateData["password"] = bcrypt($request->password);
        $validateData["role_id"]  = 2;

        $user = User::create($validateData);
        $user->sendEmailVerificationNotification();

        $token = auth()->tokenById($user->id);

        return (new AuthResource(['user' => $user, 'access_token' => $token]))->response()
                                                                            ->setStatusCode(Response::HTTP_CREATED);
    }


     /**
     * Login
     *
     * Login Method to allow users to use all available endpoints through an access token
     *
     * @bodyParam email string required The email of the user. Example: mariano.paz.flores@gmail.com
     * @bodyParam password string required The password of the user.
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     *      "data": {
     *          "user": {
     *              "resource": 12,
     *              "full_name": "mariansopfaasdfa",
     *              "email": "mariaasdnoasdfa1asdfa2@marianosa.com",
     *              "created_at": "2020-06-13 14:44:30",
     *              "updated_at": "2020-06-13 14:44:30",
     *              "links": [
     *                  {
     *                      "rel": "self",
     *                      "href": "http://localhost:8000/api/users/12"
     *                  }
     *               ]
     *          },
     *          "access_token": "eyJ0eXAiOiJKV1QiLC..."
     *      }
     * }
     * @response status=422 scenario="Validation Failed" {
     *  "error": {
     *      "name": [
     *          "The name field is required."
     *      ],
     *      "email": [
     *          "The email field is required."
     *      ],
     *      "password": [
     *          "The password field is required."
     *      ]
     *   },
     *   "code": 422
     * }
     *
     */
    public function login(LoginRequest $request)
    {
        $loginData = $request->validated();
        if (!$token = auth()->attempt($loginData)) {
            return $this->errorResponse("Invalid credentials", Response::HTTP_UNAUTHORIZED);
        }

        return new AuthResource(['user' => auth()->user(), 'access_token' => $token]);
    }


    /**
     *
     * Logout
     *
     * Logout for aunthenticated users
     *
     * @authenticated
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     *      data {
     *          'message':'Successfully logged out'
     *      }
     * }
     *
     */
    public function logout()
    {
        auth()->logout();
        return $this->successResponse(['message' =>'Successfully logged out']);
    }


    /**
     * Refresh
     *
     * Refresh token
     *
     * @authenticated
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     *      data {
     *          'token':'eyJ0eXAiOiJKV1QiLC...'
     *      }
     * }
     *
     * @response status=401 scenario="Unauthenticated" {
     *  "error": {
     *     "error": "Unauthenticated.",
     *     "code": 401
     * }
     *
     */

    public function refresh()
    {
        $newToken = auth()->refresh();
        return $this->successResponse(['token' => $newToken]);
    }


    /**
     * Me
     *
     * Get the authenticated User.
     *
     * @authenticated
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     *      data: {
     *           "resource": 3,
     *           "full_name": "Bernard Abshire",
     *           "email": "lavon.stiedemann@example.org",
     *           "role": "Admin",
     *           "creator": "System",
     *           "editor": "System",
     *           "created_at": "2020-06-12 21:44:12",
     *           "updated_at": "2020-06-12 21:44:12",
     *           "links": [
     *               {
     *                   "rel": "self",
     *                   "href": "http://localhost:8000/api/users/3"
     *               }
     *           ]
     *       }
     * }
     *
     *
     * @response status=401 scenario="Unauthenticated" {
     *  "error": {
     *     "error": "Unauthenticated.",
     *     "code": 401
     * }
     */
    public function me()
    {
        return new UserResosurce(auth()->user());
    }

}
