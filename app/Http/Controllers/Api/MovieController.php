<?php

namespace App\Http\Controllers\Api;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Movies\MovieResource;
use App\Http\Resources\Movies\MovieCollection;
use App\Http\Requests\Movies\MovieStoreRequest;
use App\Http\Requests\Movies\MovieUpdateRequest;

/**
 * @group Movie endpoints
 *
 * Movie methods available:
 *
 * For Regular Users, Admin and Guests: Index, Show
 * For Admin Users only: Store, Update and Destroy
 *
 */
class MovieController extends ApiController
{

    public function __construct()
    {
        $this->middleware('transform.fields:'. MovieResource::class)->only(["store", "update"]);
    }


    /**
     * Listing Movies
     *
     * Display a listing of movies with pagination, filters, searching, etc.
     *
     * @queryParam sortBy to sort by. Defaults to 'name'.
     * @queryParam sortByDesc to sort by desc
     * @queryParam per_page to paginate the listing. Default to 15
     * @queryParam filters: add the name of the field follow by colon and the value. Example: name="Autem"
     *
     * @response 200 {
     *      "data": {
     *           {
     *               "resource": 33,
     *               "name": "Autem placeat voluptatibus fugit.",
     *               "synopsis": "Ipsum ullam dignissimos incidunt. Eligendi a consequuntur ipsa illum et quam vero. Voluptatibus animi sed veritatis repellat at repellendus dignissimos voluptas. Error corrupti sed est corporis esse.",
     *               "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
     *               "likes": 2,
     *               "rent_daily": 1.8,
     *               "price_sale": 7.6,
     *               "penalty_per_day": 1.5,
     *               "created_at": "2020-06-12 21:44:12",
     *               "updated_at": "2020-06-12 21:44:12",
     *               "links": [
     *                   {
     *                       "rel": "self",
     *                       "href": "http:\/\/localhost\/api\/movies\/33"
     *                   }
     *               ]
     *           },
     *           {
     *               "resource": 22,
     *               "name": "Debitis dolore totam labore omnis asperiores dolore necessitatibus.",
     *               "synopsis": "Nulla eos doloremque rerum maiores. Veniam qui repellendus deleniti nemo provident. Quia quod soluta blanditiis nihil quos et. Vero voluptas officia voluptatum consequatur. Et veniam doloribus quia aut. Et quis explicabo voluptatibus accusamus voluptatem est.",
     *               "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
     *               "likes": 0,
     *               "rent_daily": 1.8,
     *               "price_sale": 7.1,
     *               "penalty_per_day": 1.4,
     *               "created_at": "2020-06-12 21:44:12",
     *               "updated_at": "2020-06-12 21:44:12",
     *               "links": [
     *                   {
     *                       "rel": "self",
     *                       "href": "http:\/\/localhost\/api\/movies\/22"
     *                   }
     *               ]
     *           },
     *           {
     *               "resource": 10,
     *               "name": "Distinctio eaque fugit assumenda illum.",
     *               "synopsis": "Doloribus et assumenda excepturi minima totam odio omnis. Ea numquam tempore dicta facere. Quos modi distinctio fugit quasi et consequatur. Qui sunt exercitationem quisquam eum est. Ut suscipit eum dolor in rerum laborum est. Doloremque sed atque harum iste at. Ea eum occaecati at.",
     *               "image": "http:\/\/localhost\/img\/film-poster-placeholder.png",
     *               "likes": 0,
     *               "rent_daily": 1.3,
     *               "price_sale": 5.5,
     *               "penalty_per_day": 1,
     *               "created_at": "2020-06-12 21:44:12",
     *               "updated_at": "2020-06-12 21:44:12",
     *               "links": [
     *                   {
     *                       "rel": "self",
     *                       "href": "http:\/\/localhost\/api\/movies\/10"
     *                   }
     *               ]
     *           },
     *      },
     *      "pagination": {
     *          "current_page": 1,
     *          "from": 1,
     *          "last_page": 2,
     *          "per_page": 15,
     *          "to": 15,
     *          "total": 21,
     *          "count": 15,
     *          "prev": null,
     *          "next": "http:\/\/localhost\/api\/movies?page=2",
     *          "first": "http:\/\/localhost\/api\/movies?page=1",
     *          "last": "http:\/\/localhost\/api\/movies?page=2"
     *      }
     * }
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Custom # of items per page.
        $perPage = request()->has("per_page") ? (int) request()->per_page : 15;
        $data = Movie::withCount('likes');

        //Filtering By any field including Likes and Title, using transformed data for security.
        if(request()->has('sortBy') || request()->has('sortByDesc')){
            $field = request()->sortBy ?? request()->sortByDesc;
            $mode = request()->sortBy ? "asc" : "desc";
            $attribute = MovieResource::originalAttribute(strtolower($field));
            $data->orderBy($attribute, $mode);
        } else{
            $data->orderBy("title", "asc");
        }

        // Guests and Regular users: will only see available movies
        // Admin users: will see available and unavailable movies
        $user = auth()->user();
        if(!$user || !$user->isAdmin()){
            $data->where("available", Movie::AVAILABLE);
        }

        // SEARCH: working with tranform data for security (All files including Status: Available or Unavailable)
        foreach (request()->query() as $query => $value) {
            $attribute = MovieResource::originalAttribute(strtolower($query));
            if($attribute == "available"){
                $data->where($attribute, MovieResource::transformedValues(strtolower($value)));
            }elseif(isset($attribute, $value)){
                $data->where($attribute, 'like', '%'.$value.'%');
            }
        }

        $movies = $data->paginate($perPage);

        return new MovieCollection($movies);
    }

    /**
     * Create Movie
     *
     * Store a newly created movie in storage. (Only admin users)
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @authenticated
     *
     * @response 200 {
     * "data": {
     *   "resource": 82,
     *   "name": "New Movie 2020",
     *   "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
     *   "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
     *   "likes": 0,
     *   "status": "Unavailable",
     *   "creator": "Bernard Abshire",
     *   "editor": "Bernard Abshire",
     *   "rent_daily": 1.25,
     *   "price_sale": 15.45,
     *   "penalty_per_day": 1,
     *   "created_at": "2020-06-13 15:14:34",
     *   "updated_at": "2020-06-13 15:14:34",
     *   "links": [
     *       {
     *           "rel": "self",
     *           "href": "http://localhost:8000/api/movies/82"
     *       }
     *   ]
     * }
     *
     * @response status=422 scenario="Validation Failed" {
     *  "error": {
     *       "name": [
     *          "The name has already been taken."
     *      ]
     *   },
     *   "code": 422
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieStoreRequest $request)
    {
        $this->adminAccess();

        $data = $request->all();


        if($request->hasFile('poster')){
            $data["poster"] = $request->poster->store('');
        }

        $data["available"] = MovieResource::transformedValues(strtolower($request->available));

        $movie = Movie::create($data);

        return new MovieResource($movie);
    }

    /**
     * Show Movie
     *
     * Display the specified movie.
     *
     *
     * @response status=404 scenario="Not Found" {
     *  "error": "Does not exist any instance of movie with the given id",
     *  "code": 404
     * }
     *
     * @param  Movie $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return new MovieResource($movie->loadCount('likes'));
    }

    /**
     * Update Movie
     *
     * Update the specified movie in storage. You need to at least update 1 field. (Only admin users)
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @authenticated
     *
     * @bodyParam name  string. Example: Titanic
     *
     * @bodyParam poster file
     * The value must be an image.
     *
     * @bodyParam synopsis  string. Example: Lorem ipsum dolor sit, amet consectetur adipisicing elit.
     *
     * @bodyParam status  string. Example: Available
     * The value must be one of available, unavailable, Available, or Unavailable.
     *
     * @bodyParam rent_daily  number. Example: 1.00
     *
     * @bodyParam price_sale  number. Example: 2.5
     *
     * @bodyParam penalty_per_day  number. Example: 1.25
     *
     *
     * @response 200 {
     * "data": {
     *   "resource": 82,
     *   "name": "New Movie 2020",
     *   "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
     *   "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
     *   "likes": 0,
     *   "status": "Unavailable",
     *   "creator": "Bernard Abshire",
     *   "editor": "Bernard Abshire",
     *   "rent_daily": 1.25,
     *   "price_sale": 15.45,
     *   "penalty_per_day": 1,
     *   "created_at": "2020-06-13 15:14:34",
     *   "updated_at": "2020-06-13 15:14:34",
     *   "links": [
     *       {
     *           "rel": "self",
     *           "href": "http://localhost:8000/api/movies/82"
     *       }
     *   ]
     * }
     *
     * @response status=422 scenario="Validation Failed" {
     *  "error": {
     *       "name": [
     *           "The name has already been taken."
     *       ]
     *   },
     *   "code": 422
     * }
     *
     * @response status=404 scenario="Not Found" {
     *  "error": "Does not exist any instance of movie with the given id",
     *  "code": 404
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Movie $movie
     * @return \Illuminate\Http\Response
     */
    public function update(MovieUpdateRequest $request, Movie $movie)
    {
        $this->adminAccess();

        $data = $request->all();

        if($request->hasFile('poster')){
            $data["poster"] = $request->poster->store('');
        }

        if($request->has("available")){
            $data["available"] = MovieResource::transformedValues(strtolower($request->available));
        }

        $movie->fill($data);

        if ($movie->isClean()) {
            return $this->errorResponse('You need to change at least one field', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $movie->save();
        return new MovieResource($movie);
    }

    /**
     * Delete a movie
     *
     * Remove the specified movie from storage (Only admin users)
     *
     * @authenticated
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     * "data": {
     *   "resource": 82,
     *   "name": "New Movie 2020",
     *   "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
     *   "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
     *   "likes": 0,
     *   "status": "Unavailable",
     *   "creator": "Bernard Abshire",
     *   "editor": "Bernard Abshire",
     *   "rent_daily": 1.25,
     *   "price_sale": 15.45,
     *   "penalty_per_day": 1,
     *   "created_at": "2020-06-13 15:14:34",
     *   "updated_at": "2020-06-13 15:14:34",
     *   "links": [
     *       {
     *           "rel": "self",
     *           "href": "http://localhost:8000/api/movies/82"
     *       }
     *   ]
     * }
     *
     * @response status=404 scenario="Not Found" {
     *  "error": "Does not exist any instance of movie with the given id",
     *  "code": 404
     * }
     * @param  Movie $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $this->adminAccess();
        $movie->delete();
        return new MovieResource($movie);
    }
}
