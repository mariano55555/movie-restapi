<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Rental;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Rentals\RentalUpdateRequest;
use App\Http\Resources\Rentals\RentalResosurce;

/**
 * @group Rentals(Transaction) endpoints
 *
 * Movie methods available:
 *
 * For Regular Users, Admin and Guests: Show, Update
 * For Admin Users only: Index,
 *
 */
class RentalController extends ApiController
{


    public function __construct()
    {
        parent::__construct();
        $this->middleware('can:view,rental')->only(['show', 'update']);
        $this->middleware('transform.fields:'. RentalResosurce::class)->only(["update"]);
    }

    /**
     * Listing rentals
     *
     * List of all transactions available (Only admin users)
     *
     * @authenticated
     *
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     * "data": [
     *     {
     *         "resource": 1,
     *         "transaction": "RENT",
     *         "qty": 1,
     *         "creator": "Ramona Barrows",
     *         "editor": "Ramona Barrows",
     *         "rent_date": "2020-06-12 21:44:17",
     *         "due_date": "2020-06-15 21:44:17",
     *         "return_date": "2020-06-16 04:00:15",
     *         "daily_penalty": 1,
     *         "rental_price": 1.2,
     *         "movie": {
     *             "resource": 50,
     *             "name": "Mariano Paz",
     *             "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
     *             "image": "http://localhost:8000/img/film-poster-placeholder.png",
     *             "likes": 0,
     *             "status": "Unavailable",
     *             "creator": "System",
     *             "editor": "Ramona Barrows",
     *             "rent_daily": 4.8,
     *             "price_sale": 2.2,
     *             "penalty_per_day": 1,
     *             "created_at": "2020-06-12 21:44:12",
     *             "updated_at": "2020-06-12 21:57:35",
     *             "links": [
     *                 {
     *                     "rel": "self",
     *                     "href": "http://localhost:8000/api/movies/50"
     *                 }
     *             ]
     *         },
     *         "customer": {
     *             "resource": 5,
     *             "full_name": "Mr. Sylvester Murphy V",
     *             "email": "mcclure.ross@example.org",
     *             "role": "User",
     *             "creator": "System",
     *             "editor": "System",
     *             "created_at": "2020-06-12 21:44:12",
     *             "updated_at": "2020-06-12 21:44:12",
     *             "links": [
     *                 {
     *                     "rel": "self",
     *                     "href": "http://localhost:8000/api/users/5"
     *                 }
     *             ]
     *         },
     *         "links": [
     *             {
     *                 "rel": "self",
     *                 "href": "http://localhost:8000/api/rentals/1"
     *             }
     *         ]
     *     },
     *     {
     *         "resource": 2,
     *         "transaction": "PURCHASE",
     *         "qty": 2,
     *         "creator": "Bernard Abshire",
     *         "editor": "Bernard Abshire",
     *         "sale_price": 2.2,
     *         "movie": {
     *             "resource": 50,
     *             "name": "Mariano Paz",
     *             "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
     *             "image": "http://localhost:8000/img/film-poster-placeholder.png",
     *             "likes": 0,
     *             "status": "Unavailable",
     *             "creator": "System",
     *             "editor": "Ramona Barrows",
     *             "rent_daily": 4.8,
     *             "price_sale": 2.2,
     *             "penalty_per_day": 1,
     *             "created_at": "2020-06-12 21:44:12",
     *             "updated_at": "2020-06-12 21:57:35",
     *             "links": [
     *                 {
     *                     "rel": "self",
     *                     "href": "http://localhost:8000/api/movies/50"
     *                 }
     *             ]
     *         },
     *         "customer": {
     *             "resource": 3,
     *             "full_name": "Bernard Abshire",
     *             "email": "lavon.stiedemann@example.org",
     *             "role": "Admin",
     *             "creator": "System",
     *             "editor": "System",
     *             "created_at": "2020-06-12 21:44:12",
     *             "updated_at": "2020-06-12 21:44:12",
     *             "links": [
     *                 {
     *                     "rel": "self",
     *                     "href": "http://localhost:8000/api/users/3"
     *                 }
     *             ]
     *         },
     *         "links": [
     *             {
     *                 "rel": "self",
     *                 "href": "http://localhost:8000/api/rentals/2"
     *             }
     *         ]
     *     },
     *     {
     *         "resource": 3,
     *         "transaction": "RENT",
     *         "qty": 1,
     *         "creator": "Bernard Abshire",
     *         "editor": "Bernard Abshire",
     *         "rent_date": "2020-06-13 02:20:15",
     *         "due_date": "2020-06-16 02:20:15",
     *         "return_date": "2020-06-18 04:00:12",
     *         "daily_penalty": 1.5,
     *         "rental_price": 1.6,
     *         "movie": {
     *             "resource": 12,
     *             "name": "Et rerum quaerat omnis ab est vero.",
     *             "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
     *             "image": "http://localhost:8000/img/film-poster-placeholder.png",
     *             "likes": 0,
     *             "status": "Unavailable",
     *             "creator": "System",
     *             "editor": "System",
     *             "rent_daily": 1.6,
     *             "price_sale": 6.1,
     *             "penalty_per_day": 1.5,
     *             "created_at": "2020-06-12 21:44:12",
     *             "updated_at": "2020-06-12 21:44:12",
     *             "links": [
     *                 {
     *                     "rel": "self",
     *                     "href": "http://localhost:8000/api/movies/12"
     *                 }
     *             ]
     *         },
     *         "customer": {
     *             "resource": 3,
     *             "full_name": "Bernard Abshire",
     *             "email": "lavon.stiedemann@example.org",
     *             "role": "Admin",
     *             "creator": "System",
     *             "editor": "System",
     *             "created_at": "2020-06-12 21:44:12",
     *             "updated_at": "2020-06-12 21:44:12",
     *             "links": [
     *                 {
     *                     "rel": "self",
     *                     "href": "http://localhost:8000/api/users/3"
     *                 }
     *             ]
     *         },
     *         "links": [
     *             {
     *                 "rel": "self",
     *                 "href": "http://localhost:8000/api/rentals/3"
     *             }
     *         ]
     *     }
     * ],
     * "links": {
     *     "first": "http://localhost:8000/api/rentals?page=1",
     *     "last": "http://localhost:8000/api/rentals?page=1",
     *     "prev": null,
     *     "next": null
     * },
     * "meta": {
     *     "current_page": 1,
     *     "from": 1,
     *     "last_page": 1,
     *     "path": "http://localhost:8000/api/rentals",
     *     "per_page": 15,
     *     "to": 3,
     *     "total": 3
     * }
     *}
     * @return void
     */
    public function index()
    {
        $this->adminAccess();
        $records = Rental::paginate();
        return RentalResosurce::collection($records);
    }

    /**
     * Show Rental
     *
     * Display the specified transaction.
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @authenticated
     *
     * @response 200 {
     *   "data": {
     *       "resource": 3,
     *       "transaction": "RENT",
     *       "qty": 1,
     *       "creator": "Bernard Abshire",
     *       "editor": "Bernard Abshire",
     *       "rent_date": "2020-06-13 02:20:15",
     *       "due_date": "2020-06-16 02:20:15",
     *       "return_date": "2020-06-18 04:00:12",
     *       "daily_penalty": 1.5,
     *       "rental_price": 1.6,
     *       "movie": {
     *           "resource": 12,
     *           "name": "Et rerum quaerat omnis ab est vero.",
     *           "synopsis": "Dolor provident quas aut velit quos autem asperiores dicta. Dignissimos sint qui quisquam laboriosam. Soluta sed dignissimos dignissimos quam omnis. Vitae voluptatibus repellendus et rerum odit. Perspiciatis quod consequatur non hic pariatur. Repudiandae aut dolorem repudiandae. Consequatur repellendus sunt nulla saepe ducimus inventore nihil.",
     *           "image": "http://localhost:8000/img/film-poster-placeholder.png",
     *           "likes": 0,
     *           "status": "Unavailable",
     *           "creator": "System",
     *           "editor": "System",
     *           "rent_daily": 1.6,
     *           "price_sale": 6.1,
     *           "penalty_per_day": 1.5,
     *           "created_at": "2020-06-12 21:44:12",
     *           "updated_at": "2020-06-12 21:44:12",
     *           "links": [
     *               {
     *                   "rel": "self",
     *                   "href": "http://localhost:8000/api/movies/12"
     *               }
     *           ]
     *       },
     *       "customer": {
     *           "resource": 3,
     *           "full_name": "Bernard Abshire",
     *           "email": "lavon.stiedemann@example.org",
     *           "role": "Admin",
     *           "creator": "System",
     *           "editor": "System",
     *           "created_at": "2020-06-12 21:44:12",
     *           "updated_at": "2020-06-12 21:44:12",
     *           "links": [
     *               {
     *                   "rel": "self",
     *                   "href": "http://localhost:8000/api/users/3"
     *               }
     *           ]
     *       },
     *       "links": [
     *           {
     *               "rel": "self",
     *               "href": "http://localhost:8000/api/rentals/3"
     *           }
     *       ]
     *   }
     * }
     *
     * @response status=404 scenario="Not Found" {
     *  "error": "Does not exist any instance of rentals with the given id",
     *  "code": 404
     * }
     */
    public function show(Rental $rental)
    {
        return new RentalResosurce($rental);
    }

    /**
     * Update Rental
     *
     * Update specified transaction.
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @authenticated
     *
     * @bodyParam transaction string optional Type of transaction value in <code>rent</code>, <code>RENT</code>, <code>Rent</code>,<code>Purchase</code>, <code>PURCHASE</code>, <code>purchase</code>. Example: Rent
     * @bodyParam qty number optional quantity of movies for purchasing or renting.
     *
     * @response 200 {
     *  "data": {
     *    "resource": 2,
     *    "transaction": "PURCHASE",
     *    "qty": 2,
     *    "creator": "Bernard Abshire",
     *    "editor": "Bernard Abshire",
     *    "sale_price": 2.2,
     *    "movie": {
     *        "resource": 50,
     *        "name": "Mariano Paz",
     *        "synopsis": "Cum ad consequatur quo numquam sed tempora molestiae. Est aperiam autem ut voluptas animi provident. Vel repellat dicta qui repellat. Debitis vel illo possimus assumenda debitis ut a. Nihil quae voluptate sit aperiam. Exercitationem quos distinctio animi voluptatem.",
     *        "image": "http://localhost:8000/img/film-poster-placeholder.png",
     *        "likes": 0,
     *        "status": "Unavailable",
     *        "creator": "System",
     *        "editor": "Ramona Barrows",
     *        "rent_daily": 4.8,
     *        "price_sale": 2.2,
     *        "penalty_per_day": 1,
     *        "created_at": "2020-06-12 21:44:12",
     *        "updated_at": "2020-06-12 21:57:35",
     *        "links": [
     *            {
     *                "rel": "self",
     *                "href": "http://localhost:8000/api/movies/50"
     *            }
     *        ]
     *    },
     *    "customer": {
     *        "resource": 3,
     *        "full_name": "Bernard Abshire",
     *        "email": "lavon.stiedemann@example.org",
     *        "role": "Admin",
     *        "creator": "System",
     *        "editor": "System",
     *        "created_at": "2020-06-12 21:44:12",
     *        "updated_at": "2020-06-12 21:44:12",
     *        "links": [
     *            {
     *                "rel": "self",
     *                "href": "http://localhost:8000/api/users/3"
     *            }
     *        ]
     *    },
     *    "links": [
     *        {
     *            "rel": "self",
     *            "href": "http://localhost:8000/api/rentals/2"
     *        }
     *    ]
     * }
     *}
     * @response status=404 scenario="Not Found" {
     *  "error": "Does not exist any instance of rentals with the given id",
     *  "code": 404
     * }
     */
    public function update(RentalUpdateRequest $request, Rental $rental)
    {
        $data = $request->only(["transaction_type", "qty", "return_date"]);

        if($request->has("return_date")){
            $data["return_date"] =  Carbon::parse($request->return_date)->format("Y-m-d H:i:s");
        }

        $rental->fill($data);

        if ($rental->isClean()) {
            return $this->errorResponse('You need to change at least one field', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $rental->save();

        return new RentalResosurce($rental);
    }
}
