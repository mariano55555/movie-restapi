<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Movie;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Rentals\RentalStoreRequest;
use App\Http\Requests\Rentals\RentalUpdateRequest;
use App\Http\Resources\Rentals\RentalResosurce;
use App\Models\Rental;
use Carbon\Carbon;
use Illuminate\Http\Response;

/**
 * @group Transaction endpoint
 *
 */
class UserRentalController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('transform.fields:'. RentalResosurce::class);
    }



    /**
     * Create a Rental/Transaction
     *
     * It generates a rental/purchase for an especific user
     *
     * @authenticated
     *
     * @bodyParam transaction string required Type of transaction value in <code>rent</code>, <code>RENT</code>, <code>Rent</code>,<code>Purchase</code>, <code>PURCHASE</code>, <code>purchase</code>. Example: Rent
     * @bodyParam qty number required quantity of movies for purchasing or renting.
     *
     * @header Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...
     *
     * @response 200 {
     * "data": {
     *       "resource": 4,
     *       "transaction": "RENT",
     *       "qty": 1,
     *       "creator": "Bernard Abshire",
     *       "editor": "Bernard Abshire",
     *       "rent_date": "2020-06-13T16:05:20.823672Z",
     *       "due_date": "2020-06-16T16:05:20.823672Z",
     *       "return_date": "",
     *       "daily_penalty": 1,
     *       "rental_price": 1.25,
     *       "movie": {
     *           "resource": 82,
     *           "name": "New Movie 2020",
     *           "synopsis": "Policies are classes that organize authorization logic around a particular model or resource. For example, if your application is a blog...",
     *           "image": "http://localhost:8000/img/tcXLNyy66tbOBCJo7JMhkoUeBFJR63pjLFb8mpvt.jpeg",
     *           "likes": 0,
     *           "status": "Unavailable",
     *           "creator": "Bernard Abshire",
     *           "editor": "Bernard Abshire",
     *           "rent_daily": 1.25,
     *           "price_sale": 15.45,
     *           "penalty_per_day": 1,
     *           "created_at": "2020-06-13 15:14:34",
     *           "updated_at": "2020-06-13 15:14:34",
     *           "links": [
     *               {
     *                   "rel": "self",
     *                   "href": "http://localhost:8000/api/movies/82"
     *               }
     *           ]
     *       },
     *       "customer": {
     *           "resource": 3,
     *           "full_name": "Bernard Abshire",
     *           "email": "lavon.stiedemann@example.org",
     *           "role": "Admin",
     *           "creator": "System",
     *           "editor": "System",
     *           "created_at": "2020-06-12 21:44:12",
     *           "updated_at": "2020-06-12 21:44:12",
     *           "links": [
     *               {
     *                   "rel": "self",
     *                   "href": "http://localhost:8000/api/users/3"
     *               }
     *           ]
     *       },
     *       "links": [
     *           {
     *               "rel": "self",
     *               "href": "http://localhost:8000/api/rentals/4"
     *           }
     *       ]
     *   }
     *}
     *
     * @param RentalStoreRequest $request
     * @param User $user
     * @param Movie $movie
     * @return void
     */
    public function store(RentalStoreRequest $request, User $user, Movie $movie)
    {

        $data = $request->only(["transaction_type", "qty"]);

        $data["user_id"]       = $user->id;
        $data["movie_id"]      = $movie->id;


        if($request->has("transaction_type")){
            $data["transaction_type"] = RentalResosurce::transformedValues(strtolower($request->transaction_type));
        }

        if($data["transaction_type"] == Rental::RENT_TRANSACTION){
                   $today           = Carbon::now();
             $data["due_date"]      = $today->copy()->addDay(Rental::DEFAULT_RENT_DAYS);
             $data["rental_date"]   = $today;
             $data['rental_price']  = $movie->rental_price;
             $data['daily_penalty'] = $movie->daily_penalty;
        }else{
            $data['sale_price'] = $movie->sale_price;
        }


        $rental = Rental::create($data);
        $rental->load("customer", "movie");

        return new RentalResosurce($rental);
    }

}
