<?php

namespace App\Http\Resources\Rentals;

use App\Http\Resources\User\UserResosurce;
use App\Http\Resources\Movies\MovieResource;
use App\Models\Rental;
use Illuminate\Http\Resources\Json\JsonResource;

class RentalResosurce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        return [
            'resource'    => (int) $this->id,
            'transaction' => (string) $this->transaction_type,
            'qty'         => (int) $this->qty,
             $this->mergeWhen(($user && $user->isAdmin()), [
                'creator' => (string) $this->creator ? $this->creator->name : 'System',
                'editor'  => (string) $this->editor ? $this->editor->name : 'System',
            ]),
            $this->mergeWhen(($this->transaction_type == "RENT"), [
                'rent_date'     => $this->rental_date ?? '',
                'due_date'      => $this->due_date ?? '',
                'return_date'   => $this->return_date ?? '',
                'daily_penalty' => (float)$this->daily_penalty ?? '',
                'rental_price'  => (float)$this->rental_price ?? '',
            ]),
            $this->mergeWhen(($this->transaction_type == "PURCHASE"), [
                'sale_price'   => (float)$this->sale_price ?? '',
            ]),
            'movie'       => new MovieResource($this->movie),
            'customer'    => new UserResosurce($this->customer),
            'links'      => [
                [
                    'rel' => 'self',
                    'href' => route('rentals.show', $this->id),
                ]
            ],
        ];
    }


    public static function originalAttribute($index)
    {
        $attributes = [
            'resource'      => 'id',
            'transaction'   => 'transaction_type',
            'qty'           => 'qty',
            'rent_date'     => 'rental_date',
            'due_date'      => 'due_date',
            'return_date'   => 'return_date',
            'daily_penalty' => 'daily_penalty',
            'rental_price'  => 'rental_price',
            'rent_daily'    => 'rental_price',
            'sale_price'    => 'sale_price',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id'               => 'resource',
            'transaction_type' => 'transaction',
            'qty'              => 'qty',
            'rental_date'      => 'rent_date',
            'due_date'         => 'due_date',
            'return_date'      => 'return_date',
            'daily_penalty'    => 'daily_penalty',
            'rental_price'     => 'rental_price',
            'rental_price'     => 'rent_daily',
            'sale_price'       => 'sale_price',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedValues($index)
    {
        $values = [
            'rent'     => Rental::RENT_TRANSACTION,
            'purchase' => Rental::BUY_TRANSACTION,
       ];

       return isset($values[$index]) ? $values[$index] : null;
    }

}
