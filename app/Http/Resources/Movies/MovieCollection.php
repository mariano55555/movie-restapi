<?php

namespace App\Http\Resources\Movies;


use App\Models\Movie;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\Movies\MovieResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MovieCollection extends ResourceCollection
{


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // Order the query strings for caching, because of the order of the filter fields could change.
        $url = request()->url();
        $queryParams = request()->query();
        ksort($queryParams);
        $queryString = http_build_query($queryParams);
        // Create the Url to cache.
        $fullUrl = "{$url}?{$queryString}";

        //Prepare the Response data
        $user = auth()->user();
        $movies = [
            'data' => $this->collection->transform(function($movie) use ($user){
                return new MovieResource($movie);
            }),
            'pagination' => [
                  'current_page' => $this->currentPage(),
                  'from'         => $this->firstItem(),
                  'last_page'    => $last = $this->lastPage(),
                  'per_page'     => $this->perPage(),
                  'to'           => $this->lastItem(),
                  'total'        => $this->total(),
                  'count'        => $this->count(),
                  'prev'         => $this->previousPageUrl(),
                  'next'         => $this->nextPageUrl(),
                  'first'        => $this->url(1),
                  'last'         => $this->url($last),
            ],
        ];

        //Return cache response.
       return Cache::remember($fullUrl, 30, function() use ($movies) {
           return $movies;
        });
    }


    public function withResponse($request, $response)
    {
        $jsonResponse = json_decode($response->getContent(), true);
        unset($jsonResponse['links'],$jsonResponse['meta']);
        $response->setContent(json_encode($jsonResponse));
    }
}
