<?php

namespace App\Http\Resources\Movies;

use App\Models\Movie;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        return [
            'resource' => (int) $this->id,
            'name'     => (string) $this->title,
            'synopsis' => (string) $this->description,
            'image'    => (string) url("img/{$this->poster}"),
            'likes'    => (int) $this->likes_count,
            $this->mergeWhen(($user && $user->isAdmin()), [
                'status'  => (string) ($this->available == Movie::AVAILABLE) ? "Available" : "Unavailable",
                'creator' => (string) $this->creator ? $this->creator->name : 'System',
                'editor'  => (string) $this->editor ? $this->editor->name : 'System',
            ]),
            'rent_daily'      => (float) $this->rental_price,
            'price_sale'      => (float) $this->sale_price,
            'penalty_per_day' => (float) $this->daily_penalty,
            'created_at'      => (string) $this->created_at,
            'updated_at'      => (string) $this->updated_at,
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('movies.show', $this->id),
                ]
            ],
        ];
    }


    public static function originalAttribute($index)
    {
        $attributes = [
            'resource'        => 'id',
            'name'            => 'title',
            'synopsis'        => 'description',
            'image'           => 'poster',
            'likes'           => 'likes_count',
            'status'          => 'available',
            'creator'         => 'created_by',
            'editor'          => 'updated_by',
            'rent_daily'      => 'rental_price',
            'price_sale'      => 'sale_price',
            'penalty_per_day' => 'daily_penalty',
            'created_at'      => 'created_at',
            'updated_at'      => 'updated_at'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
             'id'            => 'resource',
             'title'         => 'name',
             'description'   => 'synopsis',
             'poster'        => 'image',
             'likes_count'   => 'likes',
             'available'     => 'status',
             'created_by'    => 'creator',
             'updated_by'    => 'editor',
             'rental_price'  => 'rent_daily',
             'sale_price'    => 'price_sale',
             'daily_penalty' => 'penalty_per_day',
             'created_at'    => 'created_at',
             'updated_at'    => 'updated_at'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedValues($index)
    {
        $values = [
            'available'   => Movie::AVAILABLE,
            'unavailable' => Movie::UNAVAILABLE,
       ];

       return isset($values[$index]) ? $values[$index] : null;
    }


}
