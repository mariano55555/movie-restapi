<?php

namespace App\Http\Resources\User;

use App\Models\Role;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResosurce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        return [
            'resource'   => (int) $this->id,
            'full_name'  => (string) $this->name,
            'email'      => (string) $this->email,
            $this->mergeWhen(($user && $user->isAdmin()), [
                'role'    => (string) isset($this->role) ? $this->role->name : '',
                'creator' => (string) $this->creator ? $this->creator->name : 'System',
                'editor'  => (string) $this->editor ? $this->editor->name : 'System',
            ]),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'links'      => [
                [
                    'rel' => 'self',
                    'href' => route('users.show', $this->id),
                ]
            ],
        ];
    }
}
