<?php

namespace App\Http\Resources\Auth;

use App\Http\Resources\User\UserResosurce;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => new UserResosurce($this['user']),
            'access_token' => $this['access_token']
        ];
    }
}
