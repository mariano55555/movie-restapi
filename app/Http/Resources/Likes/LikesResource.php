<?php

namespace App\Http\Resources\Likes;

use App\Models\Movie;
use App\Http\Resources\Movies\MovieResource;
use App\Http\Resources\Movies\MovieCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class LikesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        return [
            'resource' => (int)$this->id,
            'user'     => (string)$this->name,
            $this->mergeWhen(($user && $user->isAdmin()), [
                'role'    => (string) isset($this->role) ? $this->role->name : '',
                'creator' => (string) $this->creator ? $this->creator->name : 'System',
                'editor'  => (string) $this->editor ? $this->editor->name : 'System',
            ]),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'likes'      => MovieResource::collection($this->likes),
            'links'      => [
                [
                    'rel' => 'self',
                    'href' => route('users.show', $this->id),
                ]
            ],
        ];
    }


}
