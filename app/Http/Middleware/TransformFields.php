<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\ValidationException;

class TransformFields
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $resource)
    {
        // First Part transform fields for store and update in order to use the aliases
        $transformedFields = [];
        $transformedFiles = [];

        foreach ($request->request->all() as $input => $value) {
            $transformedFields[$resource::originalAttribute($input)] = $value;
        }

        foreach ($request->files->all() as $input => $value) {
            $transformedFiles[$resource::originalAttribute($input)] = $value;
        }

        $request->replace($transformedFields);
        $request->files->replace($transformedFiles);

        $response = $next($request);

        // Second Part Transform the validation field names to work with the aliases
        if (isset($response->exception) && $response->exception instanceof ValidationException) {
            $data = $response->getData();

            $transformedErrors = [];

            foreach ($data->error as $field => $error) {
                $transformedField = $resource::transformedAttribute($field);
                $transformedErrors[$transformedField] = str_replace($field, $transformedField, $error);
            }

            $data->error = $transformedErrors;

            $response->setData($data);
        }

        return $response;
    }
}
