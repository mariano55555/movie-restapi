<?php

namespace App\Traits;

trait AdminAccess
{
    public function before($user, $ability)
    {
        if($user->isAdmin()){
            return true;
        }
    }
}
