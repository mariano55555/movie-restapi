<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Rental;
use App\Traits\AdminAccess;
use Illuminate\Auth\Access\HandlesAuthorization;

class RentalPolicy
{
    use HandlesAuthorization, AdminAccess;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Rental  $rental
     * @return mixed
     */
    public function view(User $user, Rental $rental)
    {
        return $user->id == $rental->user_id;
    }


}
