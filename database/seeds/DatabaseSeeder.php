<?php

use App\Models\Role;
use App\Models\User;
use App\Models\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        # DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::disableForeignKeyConstraints();

        Role::truncate();
        User::truncate();
        Movie::truncate();
        DB::table('likes')->truncate();

        Role::flushEventListeners();
        Movie::flushEventListeners();
        User::flushEventListeners();

        $this->call(RoleSeeder::class);
        $this->call(MovieSeeder::class);
        $this->call(UserSeeder::class);

        //Create 2 users (admin and regular)
        User::create([
            'name' => 'Admin User',
            'email' => 'admin@admin.com',
            'role_id' => 1,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'email_verified_at' => now(),
        ]);

        User::create([
            'name' => 'Regular User',
            'email' => 'user@user.com',
            'role_id' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'email_verified_at' => now(),
        ]);
    }
}
