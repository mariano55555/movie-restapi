<?php

use App\Models\User;
use App\Models\Movie;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create()->each(
            function($user){
                $movies = Movie::all()->random(mt_rand(1,10))->pluck("id");
                $user->likes()->attach($movies);
            }
        );
    }
}
