<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('title')->unique();
            $table->text('poster');
            $table->text('description');
            $table->boolean('available')->default(true);
            $table->unsignedDecimal('rental_price', 10, 2)->default(0.00);
            $table->unsignedDecimal('sale_price', 10, 2)->default(0.00);
            $table->unsignedDecimal('daily_penalty', 10, 2)->default(0.00);
            $table->nullableTimestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
