<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->bigInteger('movie_id')->unsigned()->index();
            $table->nullableTimestamps();
            $table->foreign('movie_id', 'fk_movie_liked_user_foreign')
                  ->references('id')->on('movies')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('user_id', 'fk_user_liked_movie_foreign')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
