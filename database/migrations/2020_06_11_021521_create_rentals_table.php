<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->bigInteger('movie_id')->unsigned()->index();
            $table->unsignedSmallInteger('qty')->default(1);
            $table->boolean('transaction_type')->comment('Type of transaction (0 for Rental and 1 For purchase)');
            $table->dateTime('rental_date')->nullable()->default(null);
            $table->dateTime('due_date')->nullable()->default(null);
            $table->dateTime('return_date')->nullable()->default(null);
            $table->unsignedDecimal('rental_price', 10, 2)->default(0.00)->comment('In case the prices change during the customer rents a movie and for historic information');
            $table->unsignedDecimal('sale_price', 10, 2)->default(0.00)->comment('In case the prices change during the customer rents a movie and for historic information');
            $table->unsignedDecimal('daily_penalty', 10, 2)->default(0.00)->comment('In case the prices change during the customer rents a movie and for historic information');
            $table->nullableTimestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->foreign('movie_id', 'fk_movie_rented_user_foreign')
                  ->references('id')->on('movies')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('user_id', 'fk_user_rented_movie_foreign')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals');
    }
}
