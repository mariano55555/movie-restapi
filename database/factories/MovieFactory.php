<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title'         => $faker->unique()->sentence($nbWords = 6, $variableNbWords = true),
        'poster'        => 'film-poster-placeholder.png',
        'description'   => $faker->text(350),
        'available'     => $faker->randomElement([Movie::AVAILABLE, Movie::UNAVAILABLE]),
        'rental_price'  => rand(10, 20) / 10,
        'sale_price'    => rand(50, 100) / 10,
        'daily_penalty' => rand(10, 15) / 10,
    ];
});
