<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



/**
 * Auth Routes
 */

Route::name("register")->post("/register", 'Api\AuthController@register');
Route::name("login")->post("/login", 'Api\AuthController@login');
Route::get('/refresh', 'Api\AuthController@refresh')->name("auth.refresh");
Route::get('/logout', 'Api\AuthController@logout')->name("auth.logout");
Route::post('me', 'Api\AuthController@me');

/**
 * Verify, Forgot Password and Reset Routes
 */
Route::name("forgotPassword")->post("/password/email", "Api\ForgotPasswordController@sendResetLinkEmail");
Route::name("resetPassword")->post("/password/reset", "Api\ResetPasswordController@reset");
Route::get('/email/resend', 'Api\VerificationController@resend')->name('verification.resend');
Route::get('/email/verify/{id}/{hash}', 'Api\VerificationController@verify')->name('verification.verify');


/**
 * Users for admin usage
 */

Route::apiResource('users', 'Api\UserController');

/**
 * Movies CRUD
 */

Route::apiResource('movies', 'Api\MovieController');


/**
 * Rentals
 */

Route::apiResource('rentals', 'Api\RentalController', ['only' => ['index', 'show', 'update']]);

/**
 * UserRentals
 */
Route::apiResource('users.movies.rentals', 'Api\UserRentalController', ['only' => ['store']]);


/**
 * Likes
 */

Route::apiResource('users.movies.likes', 'Api\UserLikeController', ['only' => ['store']]);
